<?php

class Zend_My_Controller extends Zend_Controller_Action
{

        public function init()
    {
        $identity = Zend_Auth::getInstance()->getIdentity();
        if (isset($identity)) {
            $this->_redirect('/auth/logout');
        }
    }

    public function indexAction()
    {
        parent::__construct();
    }

    public function changemypasswordAction(){

                       $identity = Zend_Auth::getInstance()->getIdentity();
                       $staffid= $identity['id'];


        $form = new Application_Form_ChangePassword(array('action'=>"/{$this->getRequest()->getControllerName()}/changemypassword"));


        $request = $this->getRequest();
        if (!$request) {
            $this->view->form = $form;
        }

        if (!$form->isValid) {
            $this->view->form = $form;
        }

            if ($request->isPost()) {
                $formvals = $request->getPost();
                    
                if ($form->isValid($formvals)) {

    
                   //    $formvals['oldpass'] = md5($formvals['oldpass']);
                   //    $formvals['newpass1'] = md5($formvals['newpass1']);
                  //     $formvals['newpass2'] = md5($formvals['newpass2']);
                       //$pass = $formvals['newpass1'];
                       $identity = Zend_Auth::getInstance()->getIdentity();
     
                       require_once 'UserModel.php';
                       $model = new Model_UserModel();
               
                       if($model->changepassword($formvals,$staffid)){
                           $this->view->flash = "Passord Changes Successfully";                           
                       }else{
                           $this->_redirect("{$this->getRequest()->getControllerName()}/changemypassword");
                       }
                 
                }



            }
        $this->view->forgotPass = $form;

    }
}
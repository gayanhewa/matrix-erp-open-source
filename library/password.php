<?php

/**
 * @desc Daily Random Password Generator
 * @author B3NZ0 Email: http://www.zend.com/mail_to.php?id=B3NZ0
 *
 * @param int $pass_len The length of the password
 * @param bool $pass_num Include numeric chars in the password?
 * @param bool $pass_alpha Include alpha chars in the password?
 * @param bool $pass_mc Include mixed case chars in the password?
 * @param string $pass_exclude Chars to exclude from the password
 * @return string The password
 */


     function make_daily_password($pass_len = 10, $pass_num = true, $pass_alpha = true, $pass_mc = true, $pass_exclude = '') {
        // Create the salt used to generate the password
        $salt = '';
        if ($pass_alpha) { // a-z
            $salt .= 'abcdefghijklmnopqrsefoifuw28udkaskjgzcjhewuyewurASDAtuvwxyz';
            if ($pass_mc) { // A-Z
                $salt .= strtoupper($salt);
            }
        }

        if ($pass_num) { // 0-9
            $rand = rand(0,999999999);
            $salt .= $rand;
        }

        // Remove any excluded chars from salt
        if ($pass_exclude) {
            $exclude = array_unique(preg_split('//', $pass_exclude));
            $salt = str_replace($exclude, '', $salt);
        }
        $salt_len = strlen($salt);

        // Seed the random number generator with today's seed & password's unique settings for extra randomness
       // mt_srand((int) date('y') * date('z') * ($salt_len + $pass_len));

        // Generate today's random password
        $pass = '';
        for ($i = 0; $i < $pass_len; $i++) {
            $pass .= substr($salt, mt_rand() % $salt_len, 1);
        }

        return $pass;
    }


?>
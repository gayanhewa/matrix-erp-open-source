<?php
/*
 * This class will hold all the table names and the program will access table names in the DB only
 * via this class , trough the Zend Registry object table. This is to create independency amoung table names
 * and the application to avoid maintenance issues that occur during the application's operational
 * activities.
 *
 */
class Tables{

    //Personal Profile
    public $profile_pis = 'profile_pis'; 
    public $profile_contact = 'profile_contact';
    public $profile_dependents = 'profile_dependents';
    public $profile_e_contact = 'profile_emergency_contact';
    public $profile_qual = 'profile_academic_qual';

    //General
    public $designation ='designation';
    public $jobdesc = 'jobdescription';
    public $role_assignment = 'job_role_assignment';
    public $objectives = 'objectives';
    public $recruit = 'recruitment';

    //User
    public $user = 'user'; //Login information table
    public $leave = 'user_leave';
    public $user_matrix = 'user_matrix';
    public $user_leave = 'user_leave';
    public $user_training = 'user_training';
    public $user_entitlement = 'user_entitlement';
    public $user_pay = 'user_pay';

    //Projects
    public $project_assignment = 'user_project_assignment';
    public $projec_status = 'user_project_status';
    public $project = 'project';
    public $eval = 'project_evaluation';
    public $appraisal = 'appraisal';    
    public $rating = 'rating';    

    public $objective_comment = 'objective_comments';
    public $progress_report = 'user_project_status';

    // SALT
    public $salt = 'jkasdjdqiqwu2309fajdsljfqwkjflasdkbdcasdcljkjdsh';

    public function getSalt(){
        return $this->salt;
    }
}
?>

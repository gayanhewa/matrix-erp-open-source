<?php

class ReportController extends Zend_Controller_Action {

    private $model = null;

    public function init() {
        /*
         *  Set up the model instance
         *
         */
        require_once("ReportModel.php");
        $this->model = new Model_ReportModel();

        /*
         *  Disable layout to render the Manager specific css layout
         */

        $layout = Zend_Layout::getMvcInstance();
        $layout->disableLayout();
        $layout->setLayout('manager_layout');

        $identity = Zend_Auth::getInstance()->getIdentity();
        if (!(($identity['user_type'] == 'manager'))) {

            $this->_helper->redirector('login', 'auth');
        }
    }

    public function preDispatch() {

//        if ($this->_request->isXmlHttpRequest()) {
//            $layout = Zend_Layout::getMvcInstance();
//            $layout->disableLayout();
//            $this->view->is_ajax = true;
//        }
    }

    public function indexAction() {
        
    }

    public function userlistAction() {

        //$layout->setLayout('m');        
        //print_r($this->model->getStaffList());die();
        //
        $param = $this->getRequest()->getParam('id');

        if ($this->getRequest()->isXmlHttpRequest()) {
            $layout = Zend_Layout::getMvcInstance();
            $layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $this->view->staff = $this->model->getStaffList($param);
            //die(print_r($this->model->getStaffList($param)));
            echo json_encode($this->model->getStaffList($param));
            //  $array = array('a'=>'A','b'=>'B');
        }
        //echo $param;
    }

    public function newusersAction() {
        
    }

    public function leavereportsAction() {
        require_once 'UserModel.php';
        $model = new Model_UserModel();
        $this->view->users = $model->getUserList();
        $this->view->allLeaves = $this->model->overallLeave();
        $this->view->chart_head = "Leave Summary";
        if ($this->getRequest()->isXmlHttpRequest()) {
            $layout = Zend_Layout::getMvcInstance();
            $layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $staffid = $this->_getParam('staffid');

            isset($staffid)? : null;
            //return die($staffid);
            $start_date = $this->_getParam('start_date');
            isset($start_date)? : null;
            $end_date = $this->_getParam('end_date');
            isset($end_date)? : null;

            $status = $this->_getParam('status');
            //isset($status)? : 'any';



          if($staffid != 'all'){
                $summary = $this->model->getStaffLeaveOverview($staffid);
          }else{
               $summary = $this->model->overallLeave();
          }
            

            //die(print_r($summary));
            // return die($staffid.$start_date.$end_date);

            $leaves = $this->model->getLeaveReport($staffid, $start_date, $end_date, $status);
            $count = count($leaves);
            echo json_encode(array('leaves' => $leaves, 'summary' => $summary, 'count' => $count));
            //return die($status);            
        }
    }

    public function individualleavesAction() {
        
    }

//    public function leavereportAction() {
//
//        $layout = Zend_Layout::getMvcInstance();
//        $layout->disableLayout();
//
//        $staffid = $this->_getParam('staffid');
//
//        isset($staffid)? : null;
//        //return die($staffid);
//        $start_date = $this->_getParam('start_date');
//        isset($start_date)? : null;
//        $end_date = $this->_getParam('end_date');
//        isset($end_date)? : null;
//        
//        $status = $this->_getParam('status');
//        isset($status)? : null;
//        require_once 'ReportModel.php';
//        $model = new Model_ReportModel();
//
//
//        if ($staffid != 'all') {
//            $this->view->summary = $model->getLeaveSummary($staffid, $status);
//            $this->staffid = $staffid;
//        }
//
//        // return die($staffid.$start_date.$end_date);
//        $this->view->leaves = $model->getLeaveReport($staffid, $start_date, $end_date, $status);
//        //return die($status);
//    }

    public function performanceAction() {

        require_once 'UserModel.php';
        $model = new Model_UserModel();
        $this->view->users = $model->getUserList();
        $this->view->rating = $this->model->getRatings();
        $this->view->ratingOverview = $this->model->getRatingOverview();

        // echo "<pre>";
        // print_r($this->model->getRatingOverview());die();

        if ($this->getRequest()->isXmlHttpRequest()) {
            $layout = Zend_Layout::getMvcInstance();
            $layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();

            $staffid = $this->getRequest()->getParam('staffid');
            $performance['rating'] = $this->model->getPerformanceByStaff($staffid);
            $performance['legend'] = $this->model->getRatings();
            echo json_encode($performance);
        }
    }

    public function projectprogressAction() {
        
    }

    public function logininfoAction() {

        $this->view->utype = $this->model->getUserTypeAnalysis();
        //die()
        $this->view->staff = $this->model->getLoginInfo();

        if ($this->getRequest()->isXmlHttpRequest()) {
            $layout = Zend_Layout::getMvcInstance();
            $layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();

            $status = $this->getRequest()->getParam('status');
            $type = $this->getRequest()->getParam('type');

            $login = $this->model->getLoginInfo($status, $type);

            echo json_encode($login);
        }
    }

    public function trainingscheduleAction() {

        require_once 'UserModel.php';
        $model = new Model_UserModel();
        $this->view->users = $model->getUserList();

        $this->view->trainings = $this->model->getAllTrainings();
        if ($this->getRequest()->isXmlHttpRequest()) {
            $layout = Zend_Layout::getMvcInstance();
            $layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $staffid = $this->getRequest()->getParam('staffid');
            $status = $this->getRequest()->getParam('status');
            $end = $this->getRequest()->getParam('end_date');
            $start = $this->getRequest()->getParam('start_date');
            $training = $this->model->getAllTrainingsByStaff($staffid, $status, $start, $end);

            echo json_encode($training);
        }
    }

    public function salaryreportAction() {
        require_once 'UserModel.php';
        $model = new Model_UserModel();
        $this->view->users = $model->getUserList();
        $this->view->coc = $this->model->getHrCoc();
//        $this->model->getStaffSalaryReview(234);
        if ($this->getRequest()->isXmlHttpRequest()) {
            $layout = Zend_Layout::getMvcInstance();
            $layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $staffid = $this->getRequest()->getParam('staffid');
            $start = $this->getRequest()->getParam('start_date');
            $end = $this->getRequest()->getParam('end_date');

            $reviews = $this->model->getStaffSalaryReview($staffid, $start, $end);
            echo json_encode($reviews);
        }
    }

    public function testAction() {
        $layout = Zend_Layout::getMvcInstance();
        $layout->disableLayout();
        $this->view->headScript()->appendFile('/js/charts.js');
    }

    public function createpdfAction() {
        $layout = Zend_Layout::getMvcInstance();
        $layout->disableLayout();
        // $layout->setLayout('pdf');        
        //$this->_helper->viewRenderer->setNoRender();
        $content = $this->getRequest()->getParam('id');
        $name = $this->getRequest()->getParam('name');
        require_once("dompdf/dompdf_config.inc.php");
        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
        $autoloader->pushAutoloader('DOMPDF_autoload');


        //die($top);
        $session = new Zend_Session_Namespace('data');
        $content = $session->report;
        //die($content);
        $dompdf = new DOMPDF();
        $dompdf->set_paper("a4", "landscape");
        $dompdf->load_html($session->report);
        $dompdf->render();
        $dompdf->stream("{$name}.pdf");
    }
    /*
     *  Bugs in the below functions , doesnt work as intended crashed 
     */
    public function createpdflandscapeAction() {
        $layout = Zend_Layout::getMvcInstance();
        $layout->disableLayout();
        // $layout->setLayout('pdf');        
        //$this->_helper->viewRenderer->setNoRender();
        $content = $this->getRequest()->getParam('id');
        $name = $this->getRequest()->getParam('name');
        require_once("dompdf/dompdf_config.inc.php");
        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
        $autoloader->pushAutoloader('DOMPDF_autoload');


        //die($top);
        $session = new Zend_Session_Namespace('data');
        $content = $session->report;
        //die($content);
        $dompdf = new DOMPDF();
        $dompdf->set_paper("a4", "portrait");
        $dompdf->load_html($session->report);
        $dompdf->render();
        $dompdf->stream("{$name}.pdf");
    }

    public function createsessionAction() {
        $layout = Zend_Layout::getMvcInstance();
        $layout->disableLayout();
        $content = $this->getRequest()->getParam('id');

        $title = $this->getRequest()->getParam('tit');
//        $ex = explode('-',$title);
//        $report_heading = implode(' ',$ex);
        
        //die($report_heading);
        Zend_Session::namespaceUnset('data');
        $session = new Zend_Session_Namespace('data');
        $top = $this->view->render('report/pdf/top_pdf.phtml');
        $header="<div id='header'><h1>{$title}</h1><img src='tmp/logo.gif' width='180px' height='75px' style='float:right'/></div>";
        $bot = $this->view->render('report/pdf/bot_pdf.phtml');
        $session->report = $top .$header. $content . $bot;
    }

    public function projectreportsAction() {
        //$this->model->getProjectDetails();
        if ($this->getRequest()->isXmlHttpRequest()) {
            $layout = Zend_Layout::getMvcInstance();
            $layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $params = $this->getRequest()->getParams();
            unset($params['controller']);
            unset($params['action']);
            unset($params['module']);

            echo $this->model->getProjectDetails($params);
        }
    }

    /*
     *  List of all active Projects
     */

    public function activeprojectsAction() {
        $this->view->projects = $this->model->activeProjects();
    }

    /*
     *  List of Ended / Completed Projects
     */

    public function completeprojectsAction() {
        $this->view->projects = $this->model->completeprojects();
    }

    /*
     *  List of ongoing projects
     * 
     */

    public function ongoingprojectsAction() {
        $staffid = $this->getRequest()->getParam('staffid');
        $this->view->projects = $this->model->ongoingProjects();
    }

    /*
     *  List of Projects by Staff memeber
     */

    public function projectbystaffAction() {
        require_once('UserModel.php');
        $model = new Model_UserModel();
        $this->view->users = $model->getUserList();
        //print_r($model->getUserList());
        //die();
        if ($this->getRequest()->isXmlHttpRequest()) {
            $layout = Zend_Layout::getMvcInstance();
            $layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $staffid = $this->getRequest()->getParam('staffid');
            echo json_encode($this->model->projectbyStaff($staffid));
        }
    }

    /*
     *  List of Projects by Progress Cycle
     */

    public function projectbycycleAction() {



        if ($this->getRequest()->isXmlHttpRequest()) {
            $layout = Zend_Layout::getMvcInstance();
            $layout->disableLayout();
            $this->_helper->viewRenderer->setNoRender();
            $cycle = $this->getRequest()->getParam('cycle');
            echo json_encode($this->model->projectbyCycle($cycle));
        }
    }
    
    /*
     *  Compare the completion of active projects
     */
    
    public function activeprojectcompletionAction(){
        $this->view->projects = $this->model->projectCompletion();
    }

    /*
     *  Delayed Projects
     */
    public function delayedprojectsAction(){
         $this->view->projects = $this->model->projectsDealyed();
    }
    
    /*
     *  Session Less PDF Report Generation : For non Ajaxed Report Pages
     */
    public function PDFAction() {
        $layout = Zend_Layout::getMvcInstance();
        $layout->disableLayout();
        // $layout->setLayout('pdf');        
        $this->_helper->viewRenderer->setNoRender();
        require_once("dompdf/dompdf_config.inc.php");
        $autoloader = Zend_Loader_Autoloader::getInstance(); // assuming we're in a controller
        $autoloader->pushAutoloader('DOMPDF_autoload');
        $content = $session->report;
        //die($content);
        $dompdf = new DOMPDF();
        $dompdf->set_paper("a4", "portrait");
        $dompdf->load_html($session->report);
        $dompdf->render();
        $dompdf->stream("{$name}.pdf");
    }
}

?>
<?php

class AuthController extends Zend_Controller_Action {

    public function init() {
        /*
         *  Disable the layout for the login page
         */
        $layout = Zend_Layout::getMvcInstance();
        $layout->disableLayout();
    }

    private function _getAuthAdapter() {
        /*
         *  Prepare the authentication adapter
         */
        $db = Zend_Registry::get('db');
        $authAdapter = new Zend_Auth_Adapter_DbTable($db);
        $tables = Zend_Registry::get('tables');
        $authAdapter
                ->setTableName("{$tables->user}")
                ->setIdentityColumn('username')
                ->setCredentialColumn('password');

        return $authAdapter;
    }

    public function indexAction() {
        /*
         *  Render the view 
         */
        if (!Zend_Auth::getInstance()->hasIdentity()) {
            $this->_redirect('/auth/login');
        }
    }

    public function loginAction() {
        /*
         *  Process the login
         */

        //Create an instance of the Login Form
        $form = new Application_Form_Login(array('action' => '/auth/login', 'method' => 'post'));

        //Get the request object
        $request = $this->getRequest();


        if (!$request->isPost) {

            $this->view->form = $form;
        }



        //Get authentication adapter
        $adapter = $this->_getAuthAdapter();

        //Get the POST values
        $formvals = $request->getPost();

        //Validate the form data
        if (isset($formvals['username']) && $formvals['username'] != '') {

            //Get an instance of the table class 
            $tables = Zend_Registry::get('tables');

            $pass = md5($formvals['password'].$tables->salt);

            //Set the values
            $adapter->setIdentity($formvals['username']);
            $adapter->setCredential($pass);
            
            //Auth object instance
            $auth = Zend_Auth::getInstance();
            //Authenticate
            $result = $auth->authenticate($adapter);

            if ($auth->hasIdentity()) {
                //Successfully logged in, now get the user type ;)
                $username = $formvals['username'];
                $auth->getStorage()->write(array('username'=>$username));
                require_once 'UserModel.php';
                $model = new Model_UserModel();
                $result = $model->getUserType($username);
                $usertype = $result['user_type'];
                switch ($usertype) {
                    case "admin":
                        $this->_redirect('/admin/index');
                        break;
                    case "hr_admin":
                        $this->_redirect('/hrmanager/index');
                        break;
                    case "manager":
                        $this->_redirect('/manager/index');
                        break;
                    case "user":
                        $this->_redirect('/user/index');
                        break;
                    default:
                        echo "NOT A VALID USER TYPE";
                }
            } else {
                $msg = $result->getMessages();
                $this->view->error = $msg[0];
            }
        }
    }

    public function logoutAction() {
        //Clear the auth objects
        $auth = Zend_Auth::getInstance();
        if ($auth->hasIdentity()) {
            $auth->clearIdentity();
        }
        $this->_helper->redirector('login', 'auth');
    }
    private function testAction(){
        //Bullet proof the random pass generator
        $passw = array();
        $pass = Zend_Registry::get('password');
        for($i=0;$i<10000;$i++){
        $tmp = $pass->make_daily_password();
        if(count($passw)>0 && in_array($tmp, $passw)){
            echo "<h3>Duplicated</h3>";
        }
        $passw[]= $tmp;

        }
        echo "<pre>";
        print_r($passw);
    }
    public function validate($username,$password)
    {
            $adapter->setIdentity($username);
            $adapter->setCredential(md5($password));
            $auth = Zend_Auth::getInstance();
            $result = $auth->authenticate($adapter);
            if($auth->hasIdentity()){
                $identity = Zend_Auth::getInstance()->getIdentity();
               echo Zend_Jason::encode($identity['staffif']);
            }
    }


}

?>

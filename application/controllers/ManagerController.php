<?php

require_once('Zend_My_Controller.php');

class ManagerController extends Zend_My_Controller {

    public function init() {

        $layout = Zend_Layout::getMvcInstance();
        $layout->disableLayout();
        $layout->setLayout('manager_layout');

        $identity = Zend_Auth::getInstance()->getIdentity();
        if (!(($identity['user_type'] == 'hr_admin') | ($identity['user_type'] == 'admin') | ($identity['user_type'] == 'manager'))) {

            //   $this->_helper->redirector('login', 'auth');
        }
    }

    public function indexAction() {

    }

    /*
     * Project Manager Functionality
     *
     */

    public function createprojectAction() {
        
        $form = new Application_Form_Project();

        $request = $this->getRequest();

        if (!$request) {
            $this->view->form = $form;
        }

        if (!$form->isValid) {
            $this->view->form = $form;
        }

        if ($request->isPost()) {

            $formvals = $request->getPost();




            if ($form->isValid($formvals)) {
                if ($formvals['start_date'] <= $formvals['end_date']) {
                    //print_r($formvals);
                    require_once 'ProjectModel.php';
                    $model = new Model_ProjectModel();

                    $result = $model->addProject($formvals);
                    if ($result != true) {
                        $this->view->form = $form;
                    } else {
                        $this->_helper->redirector('viewprojects', 'manager');
                        if ($result == 'Already Assigned') {
                            $this->view->msg = 'Already Assigned';
                        }
                    }
                } else {

                    $this->view->falsh = "Start Date must be on or before the End Date";
                    $this->view->form = $form;
                }
            }
        }
    }

    public function viewprojectsAction() {

        require_once 'ProjectModel.php';
        $model = new Model_ProjectModel();
        $this->view->proid = $this->_getParam('id');
        $mode = $this->_getParam('mode');
        $this->view->mode = "Project Listing";
        if ($mode == 'edit') {
            $this->view->mode = "Edit Project Details";
        }
        if ($mode == 'eval') {
            $this->view->mode = "Evaluate Projects";
        }

        $project_info = $model->viewprojects();

        $this->view->projects = $project_info;
    }

    public function assignprojectAction() {
        $form = new Application_Form_ProjectAssignment();

        $request = $this->getRequest();
        $project_id = $this->_getParam('id');

        if (!$request) {
            $this->view->form = $form;
        }

        if (!$form->isValid) {
            $this->view->form = $form;
        }

        if ($request->isPost()) {

            $formvals = $request->getPost();


            if ($form->isValid($formvals)) {
                //print_r($formvals);
                require_once 'ProjectModel.php';
                $model = new Model_ProjectModel();

                $result = $model->assignProject($formvals, $project_id);
                if ($result != true) {
                    $this->view->form = $form;
                } else {
                    $this->_helper->redirector('viewprojects', 'manager');
                }
            }
        }
    }

    public function viewprojectAction() {
        $id = $this->_getParam('id');

        require_once 'ProjectModel.php';
        $model = new Model_ProjectModel();
        $this->view->assignees = $model->getAssignees($id);
        $this->view->project = $model->getProjectById($id);
    }

    public function deassignAction() {
        $id = $this->_getParam('id');

        require_once 'ProjectModel.php';
        $model = new Model_ProjectModel();
        if ($model->deassign($id)) {
            $this->_redirect('/manager/viewproject/id/'.$id);
        } 
    }

    public function editprojectAction() {
        $form = new Application_Form_Project();
        $id = $this->_getParam('id');
        require_once 'ProjectModel.php';
        $model = new Model_ProjectModel();
        $result = $model->getProjectById($id);
        $request = $this->getRequest();
        $form->submit->setLabel('Update');
        if (!$request) {

            $this->view->form = $form;
        }

        if (!$form->isValid) {

            $this->view->form = $form;
        }

        if ($request->isPost()) {

            $formvals = $request->getPost();

            if ($form->isValid($formvals)) {
                //print_r($formvals);
                require_once 'ProjectModel.php';
                $model = new Model_ProjectModel();

                $result = $model->updateProject($id, $formvals);
                if ($result != true) {
                    $this->view->form = $form;
                } else {
                    $this->_helper->redirector('viewprojects', 'manager');
                }
            }
        }

        if ($result) {

            $form->populate($result);
            //print_r($result);
        }
    }

    public function removeprojectAction() {
        $id = $this->_getParam('id');

        require_once 'ProjectModel.php';
        $model = new Model_ProjectModel();
        if ($model->removeProject($id)) {
            $this->_helper->redirector('viewprojects', 'manager');
        } else {
            echo "Failed";
        }
    }

    public function evaluateprojectsAction() {
        require_once 'ProjectModel.php';
        $model = new Model_ProjectModel();

        $project_info = $model->viewprojects();
        $this->view->projects = $project_info;
    }

    public function evaluateAction() {
        $id = $this->_getParam('id');
        require_once 'ProjectModel.php';
        $model = new Model_ProjectModel();
        $result = $model->getEvaluation($id);
        //print_r($result);
        //die();
        $form = new Application_Form_EvaluateProject();

        $request = $this->getRequest();

        if (!$request) {
            $this->view->form = $form;
        }

        if (!$form->isValid) {
            $this->view->form = $form;
        }
        if ($request->isPost()) {

            $formvals = $request->getPost();


            if ($form->isValid($formvals)) {
                //   print_r($formvals);


                $result = null;
                $result = $model->addEvaluation($formvals, $id);

                if (!isset($result)) {
                    $this->view->form = $form;
                } else {
                    $this->_redirect('/manager/evaluateprojects');
                }
            }
        }
        //Populate the form is data exists
        if ($result) {
            $form->populate($result);
            //print_r($result);
        }

        $this->view->form = $form;
    }

    public function viewprogressreportsAction() {

        $id = $this->_getParam('id');
        require_once 'ProjectModel.php';
        $model = new Model_ProjectModel();
        $this->view->progress = $model->getProgressReport($id);
        $this->view->project = $model->getProjectById($id);
        $this->view->assignees = $model->getAssignees($id);
    }

    public function viewprogressreportAction() {

        $layout = Zend_Layout::getMvcInstance();
        $layout->disableLayout();
        $layout->setLayout('popup');
        $due_date = $this->_getParam('due_date');
        $id = $this->_getParam('id');

        require_once 'ProjectModel.php';
        $model = new Model_ProjectModel();

        $this->view->progress = $model->getProgressReport($id, null, date('Y-m-d', $due_date));
    }

    public function addevaluationAction() {
        $layout = Zend_Layout::getMvcInstance();
        $layout->disableLayout();
        $request = $this->getRequest();
        $formvals = $request->getPost();

        require_once 'ProjectModel.php';
        $model = new Model_ProjectModel();
        if ($model->addEval($formvals)) {
            $this->view->eval = $formvals;
        }
    }

    /*
     * Leave Manager Functionality
     */

    public function viewuserleaveAction() {

        $staffid = $this->_getParam('id');

        require_once 'UserModel.php';
        $model = new Model_UserModel();

        $page = $this->_getParam('page', 1);
        $leaveReport = $model->getPendingLeaveReport();

        $paginator_lr = Zend_Paginator::factory($leaveReport);
        $paginator_lr->setItemCountPerPage(5);
        $paginator_lr->setCurrentPageNumber($page);

        $approvedLeaveReport = $model->getApprovedLeave();
        $paginator_alr = Zend_Paginator::factory($approvedLeaveReport);
        $paginator_alr->setItemCountPerPage(5);
        $paginator_alr->setCurrentPageNumber($page);


        $this->view->leaves = $paginator_lr;
        $this->view->approvedleaves = $paginator_alr;
    }

    public function approveleaveAction() {
        $id = $this->_getParam('id');
        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];
        require_once 'UserModel.php';
        $model = new Model_UserModel();
        if ($model->approveLeave($id, $staffid)) {
            $this->_helper->redirector('viewuserleave', 'manager');
        }
    }

    public function rejectleaveAction() {
        $id = $this->_getParam('id');
        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];
        require_once 'UserModel.php';
        $model = new Model_UserModel();
        if ($model->rejectLeave($id, $staffid)) {
            $this->_helper->redirector('viewuserleave', 'manager');
        }
    }

    public function viewtrainingAction() {
        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];

        require_once 'UserModel.php';
        $model = new Model_UserModel();

        $pendingTrainings = $model->getPendingTrainings();
        // print_r($pendingTrainings);
        $page = $this->_getParam('page', 1);
        $paginator_pt = Zend_Paginator::factory($pendingTrainings);
        $paginator_pt->setItemCountPerPage(5);
        $paginator_pt->setCurrentPageNumber($page);
        $this->view->trainings = $paginator_pt;

        $approvedTrainings = $model->getApprovedTrainings();

        $page = $this->_getParam('page', 1);
        $paginator_at = Zend_Paginator::factory($approvedTrainings);
        $paginator_at->setItemCountPerPage(5);
        $paginator_at->setCurrentPageNumber($page);

        $this->view->approvedtrainings = $paginator_at;
        // print_r($approvedTrainings);
    }

    public function removeleaveAction() {



        require_once 'UserModel.php';
        $model = new Model_UserModel();

        $id = $this->_getParam('id');

        $model = new Model_UserModel();


        if ($model->removeLeave($id)) {

            $this->_helper->redirector('viewusertraining', 'manager');
        }
        //Populate the form is data exists
        if ($result) {
            $form->populate($result);
            //print_r($result);
        }

        $this->view->form = $form;
    }

    /*
     * Training Manager Functionalitys
     */

    public function approvetrainingAction() {
        require_once 'UserModel.php';
        $model = new Model_UserModel();

        $id = $this->_getParam('id');

        $model = new Model_UserModel();


        if ($model->approveTraining($id)) {

            $this->_helper->redirector('viewtraining', 'manager');
        }
    }

    public function rejecttrainingAction() {
        require_once 'UserModel.php';
        $model = new Model_UserModel();

        $id = $this->_getParam('id');

        $model = new Model_UserModel();


        if ($model->rejectTraining($id, null)) {

            $this->_helper->redirector('viewtraining', 'manager');
        }
    }

    public function removetrainingAction() {
        require_once 'UserModel.php';
        $model = new Model_UserModel();

        $id = $this->_getParam('id');

        $model = new Model_UserModel();


        if ($model->removeTraining(null, $id)) {

            $this->_helper->redirector('viewtraining', 'manager');
        }
    }

    /*
     * Appraisal Manager
     */

    public function viewappraisalsAction() {


        // print_r($pendingTrainings);


        require_once('UserModel.php');
        $model = new Model_UserModel();
        $users = $model->getUserList();
        $page = $this->_getParam('page', 1);
        $paginator_pt = Zend_Paginator::factory($users);
        $paginator_pt->setItemCountPerPage(5);
        $paginator_pt->setCurrentPageNumber($page);

        $this->view->users = $paginator_pt;
    }

    public function processappraisalAction() {
        $id = $this->_getParam('id');
        $identity = Zend_Auth::getInstance()->getIdentity();
        $manager_staffid = $identity['id'];
        $form = new Application_Form_Appraisal();

        require_once('UserModel.php');
        $model = new Model_UserModel();
        require_once('ObjectivesModel.php');
        $model_obj = new Model_ObjectivesModel();
        $appraisal = $model_obj->getObjectives($id);
        $mgr_comment = $model_obj->getManagersComment($id);
        $page = $this->_getParam('page', 1);
        $paginator_pt = Zend_Paginator::factory($mgr_comment);
        $paginator_pt->setItemCountPerPage(5);
        $paginator_pt->setCurrentPageNumber($page);

        $this->view->mgr_comment = $paginator_pt;


        //print_r($appraisal);
        $request = $this->getRequest();
        $this->view->appraisal = $appraisal;


        if ($request->isPost()) {

            $formvals = $request->getPost();


            if ($form->isValid($formvals)) {
                //print_r($formvals);
                require_once 'ProjectModel.php';
                $model = new Model_ProjectModel();

                $result = $model_obj->addManagersComment($id, $formvals, $manager_staffid);
                print_r($result);
                if ($result != true) {
                    $this->view->form = $form;
                } else {
                    $this->_helper->redirector('processappraisal', 'manager', null,
                            array(
                                'id' => $id
                    ));
                }
            }
        }

        $this->view->form = $form;
    }

    public function removecommentAction() {
        $id = $this->_getParam('id');
        $sid = $this->_getParam('sid');
        require_once('ObjectivesModel.php');
        $model = new Model_ObjectivesModel();

        $result = $model->removeManagerComment($id);
        if ($result) {
            $this->_helper->redirector('processappraisal', 'manager', null,
                    array(
                        'id' => $sid
            ));
        }
    }

    public function payAction() {
        require_once 'PayModel.php';
        $model = new Model_PayModel();
        $this->view->user = $model->getUserList();

        //die();
    }

    public function payhistoryAction() {
        $id = $this->_getParam('id');
        $this->view->id = $id;
        require_once 'PayModel.php';
        $model = new Model_PayModel();
        $this->view->user = $model->getUser($id);
    }

    public function addpayhikeAction() {
        $id = $this->_getParam('id');

        $form = new Application_Form_Pay();
        //$this->view->id = $id;
        $request = $this->getRequest();

        if (!$request) {
            $this->view->form = $form;
        }

        if (!$form->isValid) {
            $this->view->form = $form;
        }

        if ($request->isPost()) {

            $formvals = $request->getPost();


            if ($form->isValid($formvals)) {
                //   print_r($formvals);
                require_once 'PayModel.php';
                $model = new Model_PayModel();
                try {
                    //  print_r($formvals);die();
                    $result = $model->addPayhike($formvals, $id);
                    //print_r($result);die();

                    if (!$result) {
                        $this->view->form = $form;
                    } else {
                        $this->_helper->redirector('payhistory', 'manager', null, array('id' => $id));
                    }
                } catch (Exception $ex) {

                    $this->view->exception = array_pop(explode(':', $ex->getMessage()));
                    //$this->_helper->redirector('hire','hrmanager');
                }
            }
        }
    }

    public function removepayAction() {
        $id = $this->_getParam('id');
        $sid= $this->_getParam('sid');
        require_once 'PayModel.php';
        $model = new Model_PayModel();
//        echo $model->removePay($id);
//        die();
        if($model->removePay($id)){
    
            $this->_helper->redirector('payhistory', 'manager', null, array('id' => $sid));
        }   
    }

}


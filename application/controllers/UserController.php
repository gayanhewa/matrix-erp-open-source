<?php

require_once('Zend_My_Controller.php');

class UserController extends Zend_My_Controller
{

    public function init()
    {
        $layout = Zend_Layout::getMvcInstance();
        $layout->disableLayout();
        $layout->setLayout('user_layout');

        $identity = Zend_Auth::getInstance()->getIdentity();
        if ($identity['user_type'] != 'user') {
            $this->_redirect('/auth/logout');
        }
    }

    public function indexAction()
    {
        $date = new Zend_Date();
            $year = $date->getYear();
            echo $year;
    }

    //Show user profiles
    public function viewprofileAction()
    {
        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];

        require_once 'UserModel.php';
        $model = new Model_UserModel();

        $request = $this->getRequest();

        $this->view->form_pis = $form = new Application_Form_Profile();
        $this->view->form_dependents = $form_dependents = new Application_Form_DependentProfile();
        $this->view->form_e_contacts = $form_e_contact = new Application_Form_EmergencyProfile();      
        $this->view->form_qual = $form_qual = new Application_Form_QualificationsProfile();
        $this->view->form_contact = $form_contact = new Application_Form_ProfileContact();

        $this->view->pis = $model->getUserInfo($staffid,'pis');
        $this->view->dependents = $model->getUserInfo($staffid,'dependents');
        $this->view->e_contact = $model->getUserInfo($staffid,'e_contact');;
        $this->view->qual = $model->getUserInfo($staffid,'qual');
        $this->view->contact = $model->getUserInfo($staffid,'contact');
         
    }

    //Show user leave rquests
    public function viewuserleaveAction()
    {
        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];

        require_once 'UserModel.php';
        $model = new Model_UserModel();

        $this->view->leaves = $model->getLeaveReport($staffid);
        $this->view->approvedleaves = $model->getApprovedLeave($staffid);
    }

    //Add new user information
    public function adduserinfoAction()
    {
        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];

        require_once 'UserModel.php';
        $model = new Model_UserModel();
        $type = $this->_getParam('type');
        //Get the form content
        $result = $model->getUserInfo($staffid,$type);
        //Select the for that is required
        $form = $this->selectForm($type,'add');

       switch ($type){
            case 'pis':
                $this->view->title = "Add Perosnal Information";
                break;
            case 'contact':
               $this->view->title = "Add Contact Information";
                break;
            case 'dependents':
               $this->view->title = "Add Dependent Information";
                break;
            case 'e_contact':
            $this->view->title = "Add Emergency Contact Information";
                break;
            case 'qual':
            $this->view->title = "Add Acedemic Qualification Information";
                break;

        }
        $form->submit->setLabel('Save');

        $request = $this->getRequest();

        if($request->isPost()){
            $formvals = $request->getPost();
            if($form->isValid($formvals)){


                    if($model->addUserInfo($staffid, $formvals,$type))
                    {
                        $this->_helper->redirector('viewprofile','user');

                    }else{
                        echo "Failed";
                    }
                

            }
        }
        $this->view->form = $form;        
    }

    //Update existing user record
    public function updateuserinfoAction()
    {

        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];

        require_once 'UserModel.php';
        $model = new Model_UserModel();
        $type = $this->_getParam('type');
        $id = $this->_getParam('id');
        //Get the form content
        $result = $model->getUserInfo($staffid,$type,$id);
        //Select the for that is required
        $form = $this->selectForm($type,'update',$id);
       switch ($type){
            case 'pis':
                $this->view->title = "Update Perosnal Information";
                break;
            case 'contact':
               $this->view->title = "Update Contact Information";
                break;
            case 'dependents':
               $this->view->title = "Update Dependent Information";
                break;
            case 'e_contact':
            $this->view->title = "Update Emergency Contact Information";
                break;
            case 'qual':
            $this->view->title = "Update Acedemic Qualification Information";
                break;

        }

        $form->submit->setLabel('Save');

        $request = $this->getRequest();
        
        if(!$request)
        {
            //Not a valid request
        }

        if(!$form->isValid)
        {
           //Not a valid form
        }



        if($request->isPost())
        {
            $formvals = $request->getPost();
            //print_r($formvals);
            if($form->isValid($formvals)) {
                if($result)
                {
                    if($model->updateUserInfo($staffid, $formvals,$type,$id))
                    {
                        $this->_helper->redirector('viewprofile','user');
                    }
                }
              
            }
        }

        
        //Populate the form is data exists
        if($result)
        {
            $this->populateForm($result,$type,$form);
            //print_r($result);
        }

        $this->view->form = $form;


    }

    //Populate the form data
    private function populateForm($result,$type,$form)
    {
       switch ($type){
            case 'pis':
                             unset($result['id']);
                             unset($result['staffid']);
                             return $form->populate($result);
                break;
            case 'contact':
                             unset($result['id']);
                             unset($result['staffid']);
                             
                             return $form->populate($result);
                break;
            case 'dependents':
                             unset($result['id']);
                             unset($result['staffid']);
                             return $form->populate($result);
                break;
            case 'e_contact':
                             unset($result['id']);
                             unset($result['staffid']);
                             return $form->populate($result);
                break;
            case 'qual':
                             unset($result['id']);
                             unset($result['staffid']);
                             return $form->populate($result);
                break;

        }
    }

    //Select the relevant form
    private function selectForm($type,$action,$id='')
    {
         switch ($type){
            case 'pis':
                   return new Application_Form_Profile(array('action'=>'/user/'.$action.'userinfo/type/'.$type,'method'=>'post'));
                break;
            case 'contact':
                   return new Application_Form_ProfileContact(array('action'=>'/user/'.$action.'userinfo/type/'.$type.'/id/'.$id,'method'=>'post'));
                break;
            case 'dependents':
                   return new Application_Form_DependentProfile(array('action'=>'/user/'.$action.'userinfo/type/'.$type.'/id/'.$id,'method'=>'post'));
                break;
            case 'e_contact':
                   return new Application_Form_EmergencyProfile(array('action'=>'/user/'.$action.'userinfo/type/'.$type.'/id/'.$id,'method'=>'post'));
                break;
            case 'qual':
                   return new Application_Form_QualificationsProfile(array('action'=>'/user/'.$action.'userinfo/type/'.$type.'/id/'.$id,'method'=>'post'));
                break;

        }
    }

    //Request a new leave
    public function requestleaveAction()
    {
        
        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];

        require_once 'UserModel.php';
        $model = new Model_UserModel();

        $form = new Application_Form_UserLeave(array('action'=>'/user/requestleave','method'=>'post'));

        $request = $this->getRequest();

        if(!$request->getPost())
        {
            //Not a valid request
          
            $this->view->form = $form;
        }

        if(!$form->isValid)
        {
           //Not a valid form
             $this->view->form = $form;
        }

        $formvals = $request->getPost();
        //print_r($formvals);
        if($request->isPost())
        {
            if($form->isValid($formvals)){

              if($formvals['from']<= $formvals['to']){
                if($model->requestLeave($formvals,$staffid))
                {
                   $this->_helper->redirector('viewuserleave','user');
                }
              }else{
                  $this->view->date_error = "FROM date field must be on or before TO field";
                  $this->view->form = $form;
              }
            }
        }
        



    }

    public function updateleaveAction()
    {
        /*
         * TO DO @
         *
         * Validate To > From is required.
         */
        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];

        require_once 'UserModel.php';
        $model = new Model_UserModel();
    
        $id = $this->_getParam('id');
        
        //Get the form content
        $result = $model->getLeaveReport($staffid, $id);
        //Select the for that is required
        $form = new Application_Form_UserLeave();

        

        $form->submit->setLabel('Update');

        $request = $this->getRequest();

        if(!$request)
        {
            //Not a valid request
        }

        if(!$form->isValid)
        {
           //Not a valid form
        }



        if($request->isPost())
        {
            $formvals = $request->getPost();
            //print_r($formvals);
            if($form->isValid($formvals)) {
                if($result)
                {
                    if($model->upadateLeave($id, $formvals))
                    {
                        $this->_helper->redirector('viewuserleave','user');
                    }else{
                        $this->_helper->redirector('viewuserleave','user');
                    }

                }

            }
        }


        //Populate the form is data exists
        if($result)
        {

            $form->populate($result);
           // print_r($result);
        }

     $this->view->form = $form;
     

    }

    public function removeleaveAction()
    {

        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];

        require_once 'UserModel.php';
        $model = new Model_UserModel();

        $id = $this->_getParam('id');

        $model = new Model_UserModel();
        $res = $model->removeLeave($staffid,$id);
        if($res){
            if($res == 1){ $this->view->res = $res;}
            $this->_helper->redirector('viewuserleave','user');
        }else{
            echo "Failed";
        }




    }

    public function viewtrainingAction()
    {
        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];

        require_once 'UserModel.php';
        $model = new Model_UserModel();

        $this->view->trainings = $model->getTrainingReport($staffid);
    }

    public function requesttrainingAction()
     {

        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];

        require_once 'UserModel.php';
        $model = new Model_UserModel();

        $form = new Application_Form_TrainingRequest();


        $request = $this->getRequest();

        if(!$request->getPost())
        {
            //Not a valid request

            $this->view->form = $form;
        }

        if(!$form->isValid)
        {
           //Not a valid form
             $this->view->form = $form;
        }

        $formvals = $request->getPost();
        //print_r($formvals);
        if($request->isPost())
        {
            if($model->requestTraining($formvals,$staffid))
            {
               $this->_helper->redirector('viewtraining','user');
            }
        }




    }

    public function removetrainingAction()
    {

        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];

        require_once 'UserModel.php';
        $model = new Model_UserModel();

        $id = $this->_getParam('id');

        $model = new Model_UserModel();

        if($model->removeTraining($staffid,$id)){
            $this->_helper->redirector('viewtraining','user');
        }else{
            echo "Failed";
        }




    }

    public function updatetrainingAction()
    {

        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];

        require_once 'UserModel.php';
        $model = new Model_UserModel();

        $id = $this->_getParam('id');

        //Get the form content
        $result = $model->getTrainingReport($staffid, $id);
        //Select the for that is required
        $form = new Application_Form_TrainingRequest();



        $form->submit->setLabel('Update');

        $request = $this->getRequest();

        if(!$request)
        {
            //Not a valid request
        }

        if(!$form->isValid)
        {
           //Not a valid form
        }



        if($request->isPost())
        {
            $formvals = $request->getPost();
            //print_r($formvals);
            if($form->isValid($formvals)) {
                if($result)
                {
                    if($model->upadateTraining($id, $formvals))
                    {
                        $this->_helper->redirector('viewtraining','user');
                    }else{
                        $this->_helper->redirector('viewtraining','user');
                    }

                }

            }
        }


        //Populate the form is data exists
        if($result)
        {

            $form->populate($result[0]);
            //print_r($result);
        }
  
     $this->view->form = $form;


    }


    public function viewprojectsAction()
    {
        require_once 'ProjectModel.php';
        $model = new Model_ProjectModel();

        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];

        $result = $model->getProjectsByUser($staffid);

       $this->view->projects = $result;
                //print($result);
    }

    public function viewprojectAction()
    {
        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];
        $id = $this->_getParam('id');
        require_once 'ProjectModel.php';
        $model = new Model_ProjectModel();
        $this->view->progress =  $model->getProgressReport($id,$staffid);
        $this->view->assignees =  $model->getAssignees($id);
        $this->view->project = $model->getProjectById($id);
        
    }

    public function myappraisalAction()
    {
        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];

        require_once 'ObjectivesModel.php';
        $model = new Model_ObjectivesModel();
        $this->view->obj = $model->viewobjectives($staffid,date('Y'));
        $comments = $model->getmycomments($staffid,date('Y'));
        if($comments)
        {
        $this->view->comment = $comments;
        }
    }

    public function addcommentAction()
    {
        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];
        $objectiveid = $this->_getParam('id');
        require_once 'ObjectivesModel.php';
        $model = new Model_ObjectivesModel();

        $form = new Application_Form_ObjectivesComment();

        $request = $this->getRequest();

        if(!$request->getPost())
        {
            //Not a valid request

            $this->view->form = $form;
        }

        if(!$form->isValid)
        {
           //Not a valid form
             $this->view->form = $form;
        }
     if($request->isPost()){
            $formvals = $request->getPost();
            if($form->isValid($formvals)){
     //   $formvals = $request->getPost();
        //print_r($formvals);
//        if($request->isPost())
//        {
            if($model->addmycomments($formvals,$staffid,$objectiveid,date('Y')))
            {
               $this->_helper->redirector('myappraisal','user');
            }
        }
     }

    }

    public function editcommentAction()
    {
        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];

        require_once 'ObjectivesModel.php';
        $model = new Model_ObjectivesModel();

        $id = $this->_getParam('id');

        //Get the form content
        $result = $model->getmycomments($staffid,date('Y'),$id);
        //Select the for that is required
        $form = new Application_Form_ObjectivesComment();



        $form->submit->setLabel('Update');

        $request = $this->getRequest();
        $formvals = $request->getPost();
        if(!$request->getPost())
        {
            $this->view->form = $form;
        }

        if(!$form->isValid($formvals))
        {
           //Not a valid form
          $this->view->form = $form;            
        }



        if($request->isPost())
        {        
                      
            //print_r($formvals);
            if($form->isValid($formvals)) {
 
                if($result)
                {
                    if($model->updateComment($id,$formvals,$staffid))
                    {
                        $this->_helper->redirector('myappraisal','user');
                    }

                }

            }
        }


        //Populate the form is data exists
        if($result)
        {

            $form->populate($result[0]);
            //print_r($result);
        }

     $this->view->form = $form;

        
    }

    public function removecommentAction()
    {
        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];

        require_once 'ObjectivesModel.php';
        $model = new Model_ObjectivesModel();

        $id = $this->_getParam('id');

        if($model->removecomment($staffid,$id)){
            $this->_helper->redirector('myappraisal','user');
        }else{
            echo "Failed";
        }
    }

    public function addprogressreportAction()
    {
        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];
        $this->view->id = $projectid = $this->_getParam('id');
        $due_date = $this->_getParam('due_date');
        require_once 'ProjectModel.php';
        $model = new Model_ProjectModel();

        $form = new Application_Form_ProgressReport();

        $request = $this->getRequest();

        if(!$request->getPost())
        {
            //Not a valid request

            $this->view->form = $form;
        }

        if(!$form->isValid)
        {
           //Not a valid form
             $this->view->form = $form;
        }

    
        //print_r($formvals);
        if($request->isPost())
        {
            $formvals = $request->getPost();
            if(!$form->isValid($formvals)){
                return $this->view->form = $form;
        }            
            if($model->addprogressreport($formvals,$staffid,$projectid,$due_date))
            {
               $model->statusUpdate($projectid); 
               $this->_helper->redirector('viewproject','user',null,
                                               array(
                                              'id'=>$projectid
                                          )
                                            );
            }else{
               $this->_helper->redirector('viewproject','user',null,
                                               array(
                                              'id'=>$projectid
                                          )
                                            );
            }
        }
    }

    public function viewprogressreportAction()
    {
        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];
        $due_date = $this->_getParam('due_date');
        $id = $this->_getParam('id');

        require_once 'ProjectModel.php';        
        $model = new Model_ProjectModel();

        $this->view->progress =  $model->getProgressReport($id,$staffid,$due_date);
    }

    public function editprogressreportAction()
    {
        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];
        $due_date = $this->_getParam('due_date');

        require_once 'ProjectModel.php';
        $model = new Model_ProjectModel();

        $id = $this->_getParam('id');
        
        //die($due_date);
        //Get the form content
        $result = $model->getProgressReport($id, $staffid,$due_date);
        //die(print_r($result));
        if(!$result){
            
        }
        //Select the for that is required
        $form = new Application_Form_ProgressReport();



        $form->submit->setLabel('Update');

        $request = $this->getRequest();

        if(!$request)
        {
            //Not a valid request
        }

        if(!$form->isValid)
        {
           //Not a valid form
        }



        if($request->isPost())
        {
            $formvals = $request->getPost();
            //print_r($formvals);
            if($form->isValid($formvals)) {
                if($result)
                {
                    $formvals['due_date'] = $due_date;
                    if($model->updateProgress($staffid,$id,$formvals))
                    {
                        $this->_helper->redirector('viewproject','user',null,
                                               array(
                                              'id'=>$id
                                          )
                                            );
                    }

                }

            }
        }


        //Populate the form is data exists
        if($result)
        {
            
            $form->populate($result);
            //print_r($result);
        }

     $this->view->form = $form;


    }

    public function removeprogressreportAction()
    {
        $identity = Zend_Auth::getInstance()->getIdentity();
        $staffid = $identity['id'];
        $due_date = $this->_getParam('due_date');
        require_once 'ProjectModel.php';
        $model = new Model_ProjectModel();

        $id = $this->_getParam('id');

        if($model->removeprogressreport($staffid,$id,$due_date)){
             $model->statusUpdate($id); 
            $this->_helper->redirector('viewproject','user',null,
                                               array(
                                              'id'=>$id
                                          )
                                            );
        }else{
            echo "Failed";
        }
    }

   

}


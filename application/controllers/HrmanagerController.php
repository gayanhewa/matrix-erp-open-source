<?php

require_once('Zend_My_Controller.php');

class HrmanagerController extends Zend_My_Controller {

    public function init() {
        //Test the role if its a invalid role re-direct to the login page
        $identity = Zend_Auth::getInstance()->getIdentity();
        if (!(($identity['user_type'] == 'hr_admin') | ($identity['user_type'] == 'admin') | ($identity['user_type'] == 'manager'))) {

            $this->_helper->redirector('login', 'auth');
        }
    }

    public function preDispatch() {

        if ($this->_request->isXmlHttpRequest()) {
            $layout = Zend_Layout::getMvcInstance();
            $layout->disableLayout();
            $this->view->is_ajax = true;
        }
    }

    public function indexAction() {

    }

    public function userlistAction() {
        require_once 'UserModel.php';
        $model = new Model_UserModel();

        $result = $model->getUserList();
        $this->view->users = $result;

        require_once('JobdescriptionModel.php');
        $model = new Model_JobdescriptionModel();
        $this->view->list = $model->getList();
    }

    public function hireAction() {
        $form = new Application_Form_Employee();

        $request = $this->getRequest();

        if (!$request) {
            $this->view->form = $form;
        }

        if (!$form->isValid) {
            $this->view->form = $form;
        }

        if ($request->isPost()) {

            $formvals = $request->getPost();


            if ($form->isValid($formvals)) {
                //   print_r($formvals);
                require_once 'UserModel.php';
                $model = new Model_UserModel();
                try{
                    $result = null;
                    $result = $model->addProfile($formvals);
                if (!isset($result)) {
                    $this->view->form = $form;
                } else {
                $data = $result;
               //print_r($data);die();
                $config = array('ssl' => 'tls', 'port' => 587, 'auth' => 'login', 'username' => 'gayanhewa@gmail.com', 'password' => '0785348939');
                 $transport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $config);

               // $transport = new Zend_Mail_Transport_Smtp('localhost');
                Zend_Mail::setDefaultTransport($transport);
                $mail = new Zend_Mail();
                $mail->setFrom('hr@matrix-edu.com', 'Account Creation');
                $mail->addTo($data['email'], 'New User');
                $mail->setSubject('Account Activation Required');
                $mail->setBodyText('Please login to your account with your the below login information. <br/> Username : '.$data['email'].' <br/> Password :'.$data['password']);
                $mail->send();

                      $this->_helper->redirector('userlist', 'hrmanager');
                }
                }catch(Exception $ex){
                    
                    $this->view->exception = array_pop(explode(':',$ex->getMessage()));
                    //$this->_helper->redirector('hire','hrmanager');
                }
            }
        }
    }

    public function terminateAction() {
        $id = $this->_getParam('id');
        $s = $this->_getParam('s');
        require_once 'UserModel.php';
        $model = new Model_UserModel();
        $result = $model->terminateUser($id, $s);

        if (!isset($result)) {
            echo "Failed";
        } else {
            $this->_helper->redirector('userlist', 'hrmanager');
        }
    }

    public function createjobroleAction() {


        $form = new Application_Form_Jobrole();

        require_once 'JobdescriptionModel.php';
        $model = new Model_JobdescriptionModel();



        $request = $this->getRequest();

        $staffid = $this->_getParam('id');
        $jobid = $model->getUserRole($staffid);
        //print_r($jobid);
        if ($jobid) {
            $this->_helper->redirector('editjobrole', 'hrmanager'
                    , null,
                    array(
                        'id' => $staffid,
                        'jobid' => $jobid['id']
            ));
        }

        if (!$request) {
            $this->view->form = $form;
        }

        if (!$form->isValid) {
            $this->view->form = $form;
        }

        if ($request->isPost()) {

            $formvals = $request->getPost();


            if ($form->isValid($formvals)) {

                echo $result = $model->addRole($formvals, $staffid);
                if ($result != true) {
                    $this->view->form = $form;
                } else {
                    $this->_helper->redirector('userlist', 'hrmanager');
                }
            }
        }
    }

    public function editjobroleAction() {

        $form = new Application_Form_Jobrole();

        require_once 'JobdescriptionModel.php';
        $model = new Model_JobdescriptionModel();
        $form->submit->setLabel('Update Role');
        $request = $this->getRequest();
        $staffid = $this->_getParam('id');

        $result = $model->getUserRole($staffid);
        //print_r($result);
        $jobid = $result['id'];
        if (!$request) {
            $this->view->form = $form;
        }

        if (!$form->isValid) {
            $this->view->form = $form;
        }

        if ($request->isPost()) {

            $formvals = $request->getPost();


            if ($form->isValid($formvals)) {

                $result = $model->updateRole($formvals, $staffid, $jobid);
                if (!$result) {
                    $this->view->form = $form;
                } else {
                    $this->_helper->redirector('userlist', 'hrmanager');
                }
            }
        }


        //Populate the form is data exists
        if ($result) {
            $form->populate($result);
            //print_r($result);
        }

        $this->view->form = $form;
    }

    public function viewjobroleAction() {
        require_once 'JobdescriptionModel.php';
        $model = new Model_JobdescriptionModel();
        $staffid = $this->_getParam('id');
        //echo $staffid;
        $result = $model->getUserRole($staffid);
        // print_r($result);

        $this->view->jobrole = $result;
    }

    public function createobjectivesAction() {

        $form = new Application_Form_Objectives();

        $request = $this->getRequest();
        $m = new Model_UserModel();
        $this->view->staff  =  $m->getUserInfo($this->_getParam('staffid'),'pis');
        $staffid= $this->_getParam('staffid');
        
        //print_r($res);
        if (!$request) {
            $this->view->form = $form;
        }

        if (!$form->isValid) {
            $this->view->form = $form;
        }

        if ($request->isPost()) {

            $formvals = $request->getPost();


            if ($form->isValid($formvals)) {
                //print_r($formvals);
                require_once 'ObjectivesModel.php';
                $model = new Model_ObjectivesModel();
                $formvals['staffid'] = $staffid;
                $result = $model->addObjective($formvals);
                if ($result != true) {
                    $this->view->form = $form;
                } else {
                     $this->_helper->redirector('userlist', 'hrmanager');
                }
            }
        }
    }

    public function viewobjectivesAction() {

//        $layout = Zend_Layout::getMvcInstance();
//        $layout->disableLayout();

        $staffid = $this->_getParam('id');
        $name = $this->_getParam('name');
        require_once 'ObjectivesModel.php';
        $model = new Model_ObjectivesModel();
        $this->view->obj = $model->viewobjectives($staffid);
        //$this->view->name = $name;
    }

    public function removeobjectiveAction() {
        $id = $this->_getParam('id');
        require_once 'ObjectivesModel.php';
        $model = new Model_ObjectivesModel();

        $res = $model->removeobjective($id);
        if ($res) {
            $this->_helper->redirector('userlist', 'hrmanager');
        }
    }

    public function editobjectiveAction() {

        $form = new Application_Form_Objectives();
        require_once 'ObjectivesModel.php';
        $model = new Model_ObjectivesModel();
        $request = $this->getRequest();
        $id = $this->_getParam('id');
        $result = $model->getObjectiveById($id);
        if (!$request) {
            $this->view->form = $form;
        }

        if (!$form->isValid) {
            $this->view->form = $form;
        }

        if ($request->isPost()) {

            $formvals = $request->getPost();


            if ($form->isValid($formvals)) {
                //print_r($formvals);


                
                if (!$model->updateObjective($id,$formvals)) {
                    $this->view->form = $form;
                }else{
                    $this->_helper->redirector('userlist', 'hrmanager');
                }
            }
        }
        //Populate the form is data exists
        if ($result) {
            $form->populate($result);
            //print_r($result);
        }
    }

     public function changemypasswordAction() {
        $request = $this->getRequest();

        $form = new Application_Form_ChangePassword();
        if ($request->isPost) {

            $old_password = $this->_getParam('oldpass');
            $new_password = $this->_getParam('newpass');
            $identity = Zend_Auth::getInstance()->getIdentity();
            $username = $identity['username'];
            $tables = Zend_Registry::get('tables');
            $adapter->setIdentity($username);
            $adapter->setCredential(md5($old_password + $table->salt));
            $auth = Zend_Auth::getInstance();
            $result = $auth->authenticate($adapter);
            if ($result) {
                require_once('UserModel.php');
                $model = new Model_UserModel();
                if ($model->changePassword($identity['id'], $new_password)) {
                    return true;
                } else {
                    $this->view->form = $form;
                }
            }
        }

        $this->view->form = $form;
    }


    
}
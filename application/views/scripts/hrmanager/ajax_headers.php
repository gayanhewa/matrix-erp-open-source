<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="description" content="Your description goes here" />
	<meta name="keywords" content="your,keywords,goes,here" />
	<meta name="author" content="Your Name" />
	<link rel="stylesheet" type="text/css" href="/css/andreas09.css" title="andreas09" media="screen,projection" />
        <link rel="stylesheet" type="text/css" href="/css/tables.css"/>
        <link rel="stylesheet" type="text/css" href="/css/blitzer/jquery-ui-1.8.7.custom.css" />
        <script type="text/javascript" src="/js/jquery-1.4.4.js"></script>
        <script type="text/javascript" src="/js/jquery-ui-1.8.7.custom.min.js"></script>
        <link rel="stylesheet" type="text/css" href="/css/jquery.cleditor.css" />
        <script type="text/javascript" src="/js/jquery.cleditor.js"></script>
        <script type="text/javascript" src="/js/jquery.cleditor.min.js"></script>

        <script type="text/javascript">
          $(document).ready(function() {
            $("#input").cleditor({
              controls:     // controls to add to the toolbar
                        "bold underline italic | undo redo | " +
                        " | cut copy paste pastetext | print source",

            });
          });
        </script>

        <style type="text/css">
	/* This is only a demonstration of the included colors in the andreas09 template. Don't use this file as a starting point when you use the template, use the included index.html or 2col.html instead! */
	#container{background:#f0f0f0 url(/css/alt-img/bodybg-red.jpg) repeat-x;}
	#mainmenu a:hover{background:#f0f0f0 url(/css/alt-img/menuhover-red.jpg) top left repeat-x;}
	#mainmenu a.current{background:#f0f0f0 url(/css/alt-img/menuhover-red.jpg) top left repeat-x;}
	</style>
	<title>MatrixERP v1.0</title>
        <script type="text/javascript">

            $(document).ready(function(){

                $('.emp').click(function(){
                    $('.sub_emp_manager').show();
                });

                $(function() {
            	$( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
                });

             });

        </script>

<style>
    .zend_form{
    background:#fff;
    width:auto;
    margin:5px auto;
    padding:0;
    overflow:auto;
    border: 1px solid;
    }

    .zend_form dt{
    padding:0;
    clear:both;
    width:30%;
    float:left;
    text-align:right;
    margin:5px 5px 5px 0;
    }

    .zend_form dd{
    padding:0;
    float:left;
    width:68%;
    margin:5px 2px 5px 0;
    }

    .zend_form p{
    padding:0;
    margin:0;
    }

    .zend_form input, .zend_form textarea{
    margin:0 0 2px 0;
    padding:0;
    }

    #submit{
    float:right;
   /*display: none;*/
    }

  /*  .required:before{content:'* '}

    .optional:before{content:'+ '}
  */


</style>

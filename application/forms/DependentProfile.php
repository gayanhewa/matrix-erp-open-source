<?php

class Application_Form_DependentProfile extends Zend_Form
{
    public $elementDecorators = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array('Label', array('tag' => 'td')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    public $buttonDecorators = array(
        'ViewHelper',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array(array('label' => 'HtmlTag'), array('tag' => 'td', 'placement' => 'prepend')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    
    public function init()
    {
         $this->setMethod('post');


        $this->addElement(
            'text', 'full_name', array(
            'label' => 'Full Name:',
            'required' => true,
            'filters' => array('StringTrim'),
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',                
        ));

        $this->addElement(
            'text', 'relationship',array(
            'label' => 'Relationship:',
            'required' => true,
            'filters' => array('StringTrim'),
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',                
                        ));

       $this->addElement(
            'text', 'date_of_birth',array(
            'label' => 'Date of Birth:',
            'required' => true,
           
            'decorators' => $this->elementDecorators,
            'class' => 'datepicker input-text',                
        ));
      $this->addElement(
          'text',
          'date_of_birth',
        array(
              'label'          => 'Date of Birth:',
              'required'       => true,              
              'class'=>'datepicker',
               'validators' => array('date'),    
              'decorators' => $this->elementDecorators,
            'class' => 'datepicker input-text',           
                  )
      );
//       $this->addElement(
//            'text', 'nic',array(
//            'label' => 'NIC:',
//            'required' => true,
//            'filters' => array('StringTrim'),
//        ));

        $this->addElement(
            'submit', 'submit', array(
            'ignore' => true,
            'decorators' => $this->buttonDecorators,                
            'label' => 'Update',
        ));

    }
    public function loadDefaultDecorators() {
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'nostyle')),
            'Form',
        ));
    }

}


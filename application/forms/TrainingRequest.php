<?php

class Application_Form_TrainingRequest extends Zend_Form
{
   public $elementDecorators = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array('Label', array('tag' => 'td')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    public $buttonDecorators = array(
        'ViewHelper',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array(array('label' => 'HtmlTag'), array('tag' => 'td', 'placement' => 'prepend')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    public function init()
    {
      $this->setMethod('post');

       $this->addElement(
          'text',
          'due_date',
        array(
              'label'          => 'Due Date:',
              'required'       => true,
              'invalidMessage' => 'Invalid date specified.',
    
              'validators'=>array('Date'),
            'decorators' => $this->elementDecorators,
            'class' => 'datepicker input-text',   
          )
      );


        $this->addElement(
            'text', 'title', array(
            'label' => 'Title:',
            'required' => true,
            'filters' => array('StringTrim'),
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',                 
        ));

        $this->addElement(
            'textarea', 'reason', array(
            'label' => 'Why I need this training:',
            'required' => true,
            'filters' => array('StringTrim'),
                'id'=>'input',
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',                    
        ));
        $this->addElement(
            'submit', 'submit', array(
            'ignore' => true,
         'decorators' => $this->buttonDecorators,                   
            'label' => 'Request',
        ));
    
    }
    public function loadDefaultDecorators() {
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'nostyle')),
            'Form',
        ));
    }

}


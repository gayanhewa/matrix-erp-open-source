<?php

class Application_Form_ObjectivesComment extends Zend_Form
{
    public $elementDecorators = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array('Label', array('tag' => 'td')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    public $buttonDecorators = array(
        'ViewHelper',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array(array('label' => 'HtmlTag'), array('tag' => 'td', 'placement' => 'prepend')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    public function init()
    {
       $this->setMethod('post');

        $this->addElement(
             'textarea', 'comment', array(
            'label' => 'Comment:',
            'required' => true,
                 'id'=>'input',
             'validator'=>array('NotEmpty',true),
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',                
        ));

        $rating = array(
            'red'=>'Did not meet my Objectives',
            'orange'=>'I met my Objectives',
            'green'=>'Went that Extra Mile'

        );

        $this->addElement(
             'select', 'self_rating', array(
            'label' => 'Self Rating:',
            'required' => true,
             'multiOptions'=>$rating,
            'validator'=>array('NotEmpty',true),                 
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',                 
        ));

        $this->addElement(
            'submit', 'submit', array(
            'ignore' => true,
            'decorators' => $this->buttonDecorators,                
            'label' => 'Add Comment',
        ));
    }
    public function loadDefaultDecorators() {
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'nostyle')),
            'Form',
        ));
    }

}


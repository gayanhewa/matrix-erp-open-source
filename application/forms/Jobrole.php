<?php

class Application_Form_Jobrole extends Zend_Form {

    public $elementDecorators = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element','valign'=>'middle')),
        array('Label', array('tag' => 'td')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    public $buttonDecorators = array(
        'ViewHelper',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array(array('label' => 'HtmlTag'), array('tag' => 'td', 'placement' => 'prepend')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );

    public function init() {
        $this->setMethod('post');


        $this->addElement(
                'text', 'title', array(
            'label' => 'Title:',
            'required' => true,
            'filters' => array('StringTrim'),
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',
        ));

        $this->addElement(
            'textarea', 'desc', array(
            'label' => 'Job Role',
            'required' => true,
            'filters' => array('StringTrim'),
            'id' => 'input',
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',
        ));

        $this->addElement(
                'submit', 'submit', array(
            'ignore' => true,
            'decorators' => $this->buttonDecorators,
            'label' => 'Hire',
            'label' => 'Add Role',
        ));
    }

    public function loadDefaultDecorators() {
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'nostyle')),
            'Form',
        ));
    }

}


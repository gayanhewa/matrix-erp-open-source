<?php

class Application_Form_UserLeave extends Zend_Form
{
   public $elementDecorators = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array('Label', array('tag' => 'td')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    public $buttonDecorators = array(
        'ViewHelper',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array(array('label' => 'HtmlTag'), array('tag' => 'td', 'placement' => 'prepend')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    public function init()
    {
        $this->setMethod('post');

        $type = array(
            'Annual'=>'Annual',
            'Medical'=>'Medical',
            'Casual'=>'Casual'
        );

        $this->addElement(
            'select', 'type', array(
            'label' => 'Type:',
            'required' => true,
             'multiOptions' => $type,
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',                   
        ));


      $this->addElement(
          'text',
          'from',
        array(
              'label'          => 'From:',
              'required'       => true,
              'invalidMessage' => 'Invalid date specified.',
       
              'validators'=>array('Date'),
                        'decorators' => $this->elementDecorators,
            'class' => 'datepicker input-text',   

          )
      );

            $this->addElement(
          'text',
          'to',
        array(
              'label'          => 'To:',
              'required'       => true,
              'invalidMessage' => 'Invalid date specified.',
              'class'=>'datepicker',
              'validators'=>array('Date'),
            'decorators' => $this->elementDecorators,
            'class' => 'datepicker input-text',               

          )
      );

        $this->addElement(
            'text', 'reason', array(
            'label' => 'Reason:',
            'required' => true,
            'filters' => array('StringTrim'),
             'decorators' => $this->elementDecorators,
            'class' => 'input-text',   
        ));

        $this->addElement(
            'text', 'count', array(
            'label' => 'Duration:',
            'required' => true,
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',                   
                
           
        ));

        $this->addElement(
            'submit', 'submit', array(
            'ignore' => true,
            'decorators' => $this->buttonDecorators,                   
            'label' => 'Apply',
        ));
    }
    public function loadDefaultDecorators() {
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'nostyle')),
            'Form',
        ));
    }

}


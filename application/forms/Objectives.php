<?php

class Application_Form_Objectives extends Zend_Form {

    public $elementDecorators = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array('Label', array('tag' => 'td')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    public $buttonDecorators = array(
        'ViewHelper',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array(array('label' => 'HtmlTag'), array('tag' => 'td', 'placement' => 'prepend')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );

    public function init() {
        $this->setMethod('post');


        require_once('UserModel.php');
        $model = new Model_UserModel();
        $ar = $model->getUserList();
        $staffid = array();
        foreach ($ar as $d) {
            $staffid["{$d['full_name']}"] = $d['staffid'];
        }
//        $this->addElement(
//            'select', 'staffid', array(
//            'label' => 'Staff ID :',
//            'required' => true,
//            'filters' => array('StringTrim'),
//             'multiOptions' => $staffid
//        ));

        $this->addElement(
                'text', 'title', array(
            'label' => 'Title:',
            'required' => true,
            'filters' => array('StringTrim'),
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',
        ));

        $this->addElement(
                'textarea', 'desc', array(
            'label' => 'Description:',
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',
            'required' => true,
            'id' => 'input',
        ));

        $year = date('Y');
        for ($i = ($year); $i > $year - 5; $i--) {


            if ($i > $year + 10) {
                break;
            }

            $years["{$i}"] = $i;
        }
        $this->addElement(
                'select', 'year', array(
            'label' => 'Year:',
            'required' => true,
            'validators' => array('digits', array('between', false, array(1900, 3999))),
            'multiOptions' => $years,
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',
        ));
        $this->addElement(
                'submit', 'submit', array(
            'ignore' => true,
            'decorators' => $this->buttonDecorators,
            'label' => 'Add Objectives',
        ));
    }

    public function loadDefaultDecorators() {
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'nostyle')),
            'Form',
        ));
    }

}


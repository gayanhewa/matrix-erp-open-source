<?php

class Application_Form_ProfileContact extends Zend_Form
{
    public $elementDecorators = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array('Label', array('tag' => 'td')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    public $buttonDecorators = array(
        'ViewHelper',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array(array('label' => 'HtmlTag'), array('tag' => 'td', 'placement' => 'prepend')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    public function init()
    {
   $this->setMethod('post');


        $this->addElement(
            'text', 'email', array(
            'label' => 'Email ID:',
            'required' => true,
            'filters' => array('StringTrim','StringToLower',),
            'validators' => array('EmailAddress',),
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',                    
        ));

        $this->addElement(
             'text', 'mobile_phone', array(
            'label' => 'Mobile Phone #:',
            'required' => true,
            'filters' => array('StringTrim',),
            'validators' => array(array('regex', false, array('/^[+]{1}[0-9]{11}$/')),),
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',                     
        ));




        $this->addElement(
             'text', 'residence_phone', array(
            'label' => 'Residence Phone #:',
            'required' => true,
            'filters' => array('StringTrim',),
            'validators' => array(array('regex', false, array('/^[+]{1}[0-9]{11}$/')),),
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',                     
        ));




        $this->addElement(
             'textarea', 'address', array(
            'label' => 'Address:',
            'required' => true,
                 'COLS'=>'25',
                 'ROWS'=>'5',
                 'id'=>'input',
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',                     
         ));



        $this->addElement(
            'submit', 'submit', array(
            'ignore' => true,
            'decorators' => $this->buttonDecorators,                   
            'label' => 'Hire',
        ));

    /*
     *  Customer error messages for the Regex transactions
     */

    $this->getElement('mobile_phone')->addErrorMessage('Incorrect format +94xxxxxxxxxx');
    $this->getElement('residence_phone')->addErrorMessage('Incorrect format +94xxxxxxxxxx');


    }

    public function loadDefaultDecorators() {
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'nostyle')),
            'Form',
        ));
    }

}


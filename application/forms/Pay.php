<?php

class Application_Form_Pay extends Zend_Form {
    public $elementDecorators = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array('Label', array('tag' => 'td')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    public $buttonDecorators = array(
        'ViewHelper',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array(array('label' => 'HtmlTag'), array('tag' => 'td', 'placement' => 'prepend')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    public function init() {
        $this->setMethod('post');


        $this->addElement(
                'text', 'salary', array(
            'label' => 'Salary Scale:',
            'required' => true,
                //'validators' => array('mailAddress',),
                //'filter' => array('StringToLower')
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',                                
        ));

        $this->addElement(
                'text',
                'datepicker',
                array(
                    'label' => 'Date:',
                    'required' => true,
                    'invalidMessage' => 'Invalid date specified.',
     
               'validators' => array('date'),
            'decorators' => $this->elementDecorators,
            'class' => 'datepicker input-text',                                
                )
        );

        $this->addElement(
                'submit', 'submit', array(
            'ignore' => true,
            'decorators' => $this->buttonDecorators,                     
            'label' => 'Sumbit',
        ));
    }
    public function loadDefaultDecorators() {
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'nostyle')),
            'Form',
        ));
    }
}


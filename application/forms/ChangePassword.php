<?php

class Application_Form_ChangePassword extends Zend_Form {

    public $elementDecorators = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array('Label', array('tag' => 'td')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    public $buttonDecorators = array(
        'ViewHelper',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array(array('label' => 'HtmlTag'), array('tag' => 'td', 'placement' => 'prepend')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );

    public function init() {

        $this->setMethod('post');
        $this->reset();


        $this->addAttribs(array('id' => 'passresetform'));
        // Add an username element
        /*   $this->addElement('text', 'username', array(
          'label' => 'Username:',
          'required' => true,
          'filters' => array('StringTrim'),
          'class' => 'username',
          'validators' => array(
          array('validator' => 'StringLength', 'options' => array(0, 32))
          )
          ));
          // Add an password element
          $this->addElement('password', 'oldpass', array(
          'label' => 'Old Password:',
          'required' => true,
          'filters' => array('StringTrim'),
          'class' => 'oldpass',
          'validators' => array(
          array('validator' => 'StringLength', 'options' => array(0, 32))
          )
          ));
         * 
         */
        $this->addElement('password', 'newpass1', array(
            'label' => 'New Password:',
            'required' => true,
            'class' => 'newpass1',
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',
        ));

        $this->addElement('password', 'newpass2', array(
            'label' => 'Re-enter New Password:',
            'required' => true,
            'class' => 'newpass2',
            //'validators' => array(
            //    array('identical', false, array('token' => 'newpass1')))
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',
        ));
        
        // Add the submit button

        $this->addElement('submit', 'submit', array(
            'ignore' => true,
            'label' => 'Change',
            'decorators' => $this->buttonDecorators,
            'class' => 'button',
        ));
    }

    public function loadDefaultDecorators() {
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'nostyle')),
            'Form',
        ));
    }

}

?>

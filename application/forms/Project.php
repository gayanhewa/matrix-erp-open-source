<?php

class Application_Form_Project extends Zend_Form {

    public $elementDecorators = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array('Label', array('tag' => 'td')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    public $buttonDecorators = array(
        'ViewHelper',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array(array('label' => 'HtmlTag'), array('tag' => 'td', 'placement' => 'prepend')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );

    public function init() {
        $this->setMethod('post');

        $this->addElement(
                'text', 'title', array(
            'label' => 'Title:',
            'required' => true,
            'filters' => array('StringTrim'),
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',
        ));

        $this->addElement(
                'textarea', 'desc', array(
            'label' => 'Description:',
            'required' => true,
            'filters' => array('StringTrim'),
            'ROWS' => '5',
            'COLS' => '30',
            'id' => 'input',
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',
        ));

        $this->addElement(
                'text', 'start_date', array(
            'label' => 'Start Date:',
            'required' => true,
            'validators' => array('date'),
            'decorators' => $this->elementDecorators,
            'class' => 'datepicker input-text',
                )
        );

        $this->addElement(
                'text', 'end_date', array(
            'label' => 'End Date:',
            'required' => true,
            'class' => 'datepicker',
            'validators' => array('date'),
            'decorators' => $this->elementDecorators,
            'class' => 'datepicker input-text',
                )
        );

        /*
         * Progress cycle
         */


        $progress_cycle = array(
            '1' => 'Daily',
            '7' => 'Weekly',
            '14' => 'Fortnightly',
            '31' => 'Monthly'
        );
        $this->addElement(
                'select', 'progress_cycle', array(
            'label' => 'Progress Cycle:',
            'required' => true,
            'multiOptions' => $progress_cycle,
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',
        ));

        $this->addElement(
                'submit', 'submit', array(
            'ignore' => true,
            'decorators' => $this->buttonDecorators,
            'label' => 'Create',
        ));

        $this->getElement('start_date')->addErrorMessage('Invalid date specified.');
        $this->getElement('end_date')->addErrorMessage('Invalid date specified.');
    }

    public function loadDefaultDecorators() {
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'nostyle')),
            'Form',
        ));
    }

}


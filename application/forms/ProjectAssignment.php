<?php

class Application_Form_ProjectAssignment extends Zend_Form
{
   public $elementDecorators = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array('Label', array('tag' => 'td')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    public $buttonDecorators = array(
        'ViewHelper',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array(array('label' => 'HtmlTag'), array('tag' => 'td', 'placement' => 'prepend')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    public function init()
    {
        $this->setMethod('post');

        require_once('UserModel.php');
        $model = new Model_UserModel();
        $ar =  $model->getUserList();
        foreach($ar as $d){
                $staffid["{$d['staffid']}"] =  $d['staffid'] ." - ".$d['full_name'];

        }


 
        $this->addElement(
            'select', 'staffid', array(
            'label' => 'Full Name:',
            'required' => true,
            'filters' => array('StringTrim'),
            'multiOptions' => $staffid,
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',                
        ));

        $this->addElement(
            'submit', 'submit', array(
            'ignore' => true,
            'decorators' => $this->buttonDecorators,                
            'label' => 'Asign',
        ));
    }
    public function loadDefaultDecorators() {
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'nostyle')),
            'Form',
        ));
    }

}


<?php

class Application_Form_Login extends Zend_Form {

    public $elementDecorators = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array('Label', array('tag' => 'td')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    public $buttonDecorators = array(
        'ViewHelper',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array(array('label' => 'HtmlTag'), array('tag' => 'td', 'placement' => 'prepend')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );

    public function init() {

        $this->setMethod('post');
        $this->setAttrib('id', 'loginform');
        // Add an username element
        $username = $this->addElement('text', 'username', array(
                    'label' => 'Username: ',
                    'required' => true,
                    'filters' => array('StringTrim'),
                    'class' => 'inputbox',
                    'validators' => array(
                        array('validator' => 'StringLength', 'options' => array(0, 32))
                    ),
                    'decorators' => $this->elementDecorators,
                    'class' => 'input-text',
                ));
        // Add an password element
        $password = $this->addElement('password', 'password', array(
                    'label' => 'Password: ',
                    'required' => true,
                    'filters' => array('StringTrim'),
                    'class' => 'inputbox',
                    'validators' => array(
                        array('validator' => 'StringLength', 'options' => array(0, 32))
                    ),
                    'decorators' => $this->elementDecorators,
                    'class' => 'input-text',
                ));

        // Add the submit button
        $signin = $this->addElement('submit', 'submit', array(
                    'ignore' => true,
                    'decorators' => $this->buttonDecorators,
                    'label' => 'Login',
                    'class' => 'button',
                ));
    }

    public function loadDefaultDecorators()
    {
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table','class'=>'nostyle')),
            'Form',
        ));
    }   
}

?>

<?php

class Application_Form_EmergencyProfile extends Zend_Form
{
    public $elementDecorators = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array('Label', array('tag' => 'td')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    public $buttonDecorators = array(
        'ViewHelper',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array(array('label' => 'HtmlTag'), array('tag' => 'td', 'placement' => 'prepend')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    public function init()
    {
        $this->setMethod('post');


        $this->addElement(
            'text', 'full_name', array(
            'label' => 'Full Name:',
            'required' => true,
            'filters' => array('StringTrim'),
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',                   
        ));

        $this->addElement(
            'text', 'relationship',array(
            'label' => 'Relationship:',
            'required' => true,
            'filters' => array('StringTrim'),
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',                   
        ));

       $this->addElement(
            'text', 'contact_no',array(
            'label' => 'Contact No:',
             'description'=>'Format +94xxxxxxxxx',
            'required' => true,
            'filters' => array('StringTrim'),
            'validators' => array(array('regex', false, array('/^[+]{1}[0-9]{11}$/')),),
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',                   
                
        ));

        $this->addElement(
            'submit', 'submit', array(
            'ignore' => true,
            'decorators' => $this->buttonDecorators,                
            'label' => 'Update',
        ));

            /*
     *  Customer error messages for the Regex transactions
     */

    $this->getElement('contact_no')->addErrorMessage('Incorrect format +94xxxxxxxxxx');

    }
    public function loadDefaultDecorators() {
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'nostyle')),
            'Form',
        ));
    }

}


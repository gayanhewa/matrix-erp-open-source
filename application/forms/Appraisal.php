<?php

class Application_Form_Appraisal extends Zend_Form
{

    public $elementDecorators = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array('Label', array('tag' => 'td')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );

    public $buttonDecorators = array(
        'ViewHelper',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array(array('label' => 'HtmlTag'), array('tag' => 'td', 'placement' => 'prepend')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    
    public function init()
    {
       $this->setMethod('post');

        $rating = array(
              '1'=>'Excellent Performance',
              '2'=>'Good Performance',
              '3'=>'Average Performance',
              '4'=>'Needs Improvement',
              '5'=>'Poor Performance'
        );
        $this->addElement(
             'select', 'rating', array(
            'label' => 'Rating :',
            'required' => true,
             'multiOptions' => $rating,
                     'decorators' => $this->elementDecorators,    
                    'class' => 'input-text',                 
        ));

          $this->addElement(
             'text', 'comment_date', array(
            'label' => 'Comment Date :',
            'required' => true,

            'validators' => array(array('Date',)),
                     'decorators' => $this->elementDecorators,    
                    'class' => 'datepicker input-text',                 
        ));     
       $this->addElement(
             'textarea', 'managers_comment', array(
            'label' => "Manager's Comment:",
            'required' => true,       
                 'id'=>'input',
                     'decorators' => $this->elementDecorators,    
                    'class' => 'input-text',                 
        ));




        $this->addElement(
            'submit', 'submit', array(
            'ignore' => true,
           'decorators' => $this->buttonDecorators,                  
            'label' => 'Add Comment',
        ));

           
    }
    public function loadDefaultDecorators()
    {
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table','class'=>'nostyle')),
            'Form',
        ));
    }   

}


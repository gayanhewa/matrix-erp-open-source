<?php

class Application_Form_Employee extends Zend_Form {
    
    public $elementDecorators = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array('Label', array('tag' => 'td')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );

    public $buttonDecorators = array(
        'ViewHelper',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array(array('label' => 'HtmlTag'), array('tag' => 'td', 'placement' => 'prepend')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    
    public function init() {
        $this->setMethod('post');


        $staffid = $this->addElement(
                     'text', 'staffid', array(
                    'label' => 'Satff ID:',
                    'required' => true,
                    'validators' => array('digits', array('between', false,array(0, 10000))),
                     'decorators' => $this->elementDecorators,    
                    'class' => 'input-text',
                    ));

        $nic = $this->addElement(
                    'text', 'nic', array(
                    'label' => 'NIC:',
                    'required' => true,
                    'filters' => array('StringTrim', 'StringToUpper'),
                    'validators' => array('alnum', array('regex', false, array('/^[0-9]{9}[X,x,V,v]$/'))),
                     'decorators' => $this->elementDecorators,
                        'class' => 'input-text',
                ));



        require_once('DesignationModel.php');
        $model = new Model_Designation();
        $ar = $model->getAll();
        foreach ($ar as $d) {
            $designation["{$d['id']}"] = $d['Title'];
        }

        $this->addElement(
                'select', 'designation', array(
            'required' => false,
            'label' => 'Designation:', 
            'multiOptions' => $designation,
            'decorators' => $this->elementDecorators,  
                    'class' => 'input-text',
        ));

        $this->addElement(
             'text', 'salary', array(
            'label' => 'Salary Scale:',
            'required' => true,
            'validators' => array(array('between', false,array(8000, 500000))),
            //'filter' => array('StringToLower')
           'decorators' => $this->elementDecorators, 
                 'class' => 'input-text',
        ));

        $this->addElement(
                'text', 'email', array(
            'label' => 'Email:',
            'required' => true,
            'validators' => array('EmailAddress',),
            'filter' => array('StringToLower'),
            'decorators' => $this->elementDecorators,  
                    'class' => 'input-text',
        ));


        //Options
        $arr['user'] = 'User';
        $arr['manager'] = 'Executive Users';
        $arr['hr_admin'] = 'HR Manager';

        $this->addElement(
                'select', 'user_type', array(
                'required' => false,
                'label' => 'User Type:', 
                'multiOptions' => $arr,
                'decorators' => $this->elementDecorators,    
                    'class' => 'input-text',
                    
        ));

        $period = array(
            3 =>'3 Months',
            6 =>'6 Months',
            9 =>'9 Months',
            12 =>'1 Year',
            0=>'None'
        );
        $this->addElement(
                'select', 'probation', array(
            'required' => false,
            'label' => 'Probation Period:',
            'multiOptions' => $period,
             'decorators' => $this->elementDecorators,    
                    'class' => 'input-text',
        ));

        $this->addElement(
                'text',
                'datepicker',
                array(
                    'label' => 'Date:',
                    'required' => true,
                    'invalidMessage' => 'Invalid date specified.',
                    'class' => 'datepicker',
               'validators' => array('date'),
               'decorators' => $this->elementDecorators,    
                    'class' => 'datepicker input-text',
                )
                 
        );

        $this->addElement(
            'submit', 'submit', array(
            'ignore' => true,
           'decorators' => $this->buttonDecorators,         
            'label' => 'Hire',
        ));

            /*
     *  Customer error messages for the Regex transactions
     */

    $this->getElement('nic')->addErrorMessage('Incorrect format for NIC xxxxxxxxxV');
    }

    public function loadDefaultDecorators()
    {
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table','class'=>'nostyle')),
            'Form',
        ));
    }   
}


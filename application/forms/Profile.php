<?php

class Application_Form_Profile extends Zend_Form
{
    public $elementDecorators = array(
        'ViewHelper',
        'Errors',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array('Label', array('tag' => 'td')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    public $buttonDecorators = array(
        'ViewHelper',
        array(array('data' => 'HtmlTag'), array('tag' => 'td', 'class' => 'element')),
        array(array('label' => 'HtmlTag'), array('tag' => 'td', 'placement' => 'prepend')),
        array(array('row' => 'HtmlTag'), array('tag' => 'tr')),
    );
    public function init()
    {
        $this->setMethod('post');


        $this->addElement(
            'text', 'full_name', array(
            'label' => 'Full Name:',
            'required' => true,
            'filters' => array('StringTrim',),
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',                                    
        ));

        $this->addElement(
            'text', 'nic', array(
            'label' => 'NIC:',
            'required' => true,
            'filters' => array('StringTrim',),
            'validators' => array(array('regex', false, array('/^[0-9]{9}[X,x,V,v]$/')),),
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',                                    
        ));
      $this->addElement(
          'text',
          'date_of_birth',
        array(
              'label'          => 'Date of Birth:',
              'required'       => true,
              'invalidMessage' => 'Invalid date specified.',
              'class'=>'datepicker',
              'validators' => array('Date',),
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',                                
            

          )
      );

        $this->addElement(
            'text', 'nationality', array(
            'label' => 'Nationality:',
            'required' => true,
            'filters' => array('StringTrim','StringToLower',),
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',                                    
        ));
        $arr = array(
            's'=>'Single',
            'm'=>'Married'
        );
        $this->addElement(
            'select', 'marital_status', array(
            'label' => 'Marital Status:',
            'required' => true,
            'filters' => array('StringTrim','StringToLower',),
            'multiOptions'=>$arr,
            'decorators' => $this->elementDecorators,
            'class' => 'input-text',                                    
        ));


        $this->addElement(
            'submit', 'submit', array(
            'ignore' => true,
            'decorators' => $this->buttonDecorators,                 
            'label' => 'Hire',
        ));


            /*
     *  Customer error messages for the Regex transactions
     */

    $this->getElement('nic')->addErrorMessage('Incorrect format for NIC xxxxxxxxxV');
    

    }

    public function loadDefaultDecorators() {
        $this->setDecorators(array(
            'FormElements',
            array('HtmlTag', array('tag' => 'table', 'class' => 'nostyle')),
            'Form',
        ));
    }
}


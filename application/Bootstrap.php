<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    	protected function _initDb(){
		$resource = $this->getPluginResource('db');
		$db = $resource->getDbAdapter();
		Zend_Registry::set('db',$db);
	}

        protected function _initSession(){
		Zend_Session::start();	
	}

        protected function _initTables(){
                require_once('tables.php');
		$tables = new Tables();		
		Zend_Registry::set('tables',$tables);


	}

        protected function _initView(){
            $view= new Zend_View();

            $viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();

            $view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');
            $view->addHelperPath('ZendX/JQuery/View/Helper/', 'ZendX_JQuery_View_Helper');

            $viewRenderer->setView($view);

            Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);
            $view->addHelperPath(realpath(dirname(__FILE__)).'/views/helpers/');
            $view->addScriptPath(realpath(dirname(__FILE__)) . '/views/scripts/');

        }
        
	protected function _initConfig()
	{
    	$config = new Zend_Config($this->getOptions(), true);
    	Zend_Registry::set('config', $config);
    	return $config;
	}

}


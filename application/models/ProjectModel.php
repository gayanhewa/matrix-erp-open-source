<?php

class Model_ProjectModel {

    protected $db;
    protected $tables;

    function Model_ProjectModel() {
        $this->db = Zend_Registry::get('db');
        $this->tables = Zend_Registry::get('tables');
        
    }

    function addProject($data) {
        unset($data['submit']);
        $arr = $data;
        $this->db->beginTransaction();

        if ($this->db->insert($this->tables->project, $arr)) {
            $this->db->commit();
            return true;
        } else {
            $this->db->rollBack();
            return false;
        }
    }

    function viewprojects() {
        $select = $this->db->select();
        $result = $select->from(array('p'=>$this->tables->project));
                         


        return $this->db->fetchAll($result);
    }

    function assignProject($data, $project_id) {

        $staffid = $data['staffid'];
        //First check if the user is already assigned to this project
        $select = $this->db->select();
        $result = $select->from("{$this->tables->project_assignment}")
                        ->where("projectid=$project_id")
                        ->where("staffid=$staffid");
        if (count($this->db->fetchAll($result)) < 1) {

            $date = new Zend_Date();
            $date_format = 'YYYY-MM-dd HH:mm:ss';

            $arr['projectid'] = $project_id;
            $arr['staffid'] = $data['staffid'];
            $arr['assign_date'] = $date->toString($date_format);



            $this->db->beginTransaction();

            if ($this->db->insert($this->tables->project_assignment, $arr)) {
                $this->db->commit();
                return true;
            } else {
                $this->db->rollBack();
                return false;
            }
        } else {
            return 'Already Assigned';
        }
    }

    function getProjectById($id) {
        $select = $this->db->select();
        $result = $select->from(array('p' => "{$this->tables->project}"))
                        ->where($this->db->quoteInto("p.id=?", $id));


        return $this->db->fetchRow($result);
    }

    function getAssignees($id) {
        $select = $this->db->select();
        $result = $select->from(array('pa' => "{$this->tables->project_assignment}"))
                        ->join(array('p' => "{$this->tables->profile_pis}"), 'p.staffid=pa.staffid', 'full_name')
                        ->where($this->db->quoteInto("pa.projectid=?", $id));


        return $this->db->fetchAll($result);
    }

    function deassign($id) {


        $this->db->beginTransaction();

        $param = "id=" . $id;
        $query = $this->db->delete("{$this->tables->project_assignment}", $param);

        if ($query) {
            $this->db->commit();
            return true;
        } else {
            $this->db->rollBack();
            return false;
        }
    }

    function updateProject($id, $data) {
        $this->db->beginTransaction();

        unset($data['submit']);
        $query = $this->db->update("{$this->tables->project}", $data, "id=" . $id);

        if ($query) {
            $this->db->commit();
            return true;
        } else {
            $this->db->rollBack();
            return false;
        }
    }

    function removeProject($id) {
        $this->db->beginTransaction();

        $param = "id=" . $id;
        $query = $this->db->delete("{$this->tables->project}", $param);

        if ($query) {
            $this->db->commit();
            return true;
        } else {
            $this->db->rollBack();
            return false;
        }
    }

    function getProjectsByUser($staffid) {
        $select = $this->db->select();
        $result = $select->from(array('p' => "{$this->tables->project}"), array('start_date', 'title', 'id', 'end_date'))
                        ->join(array('pa' => "{$this->tables->project_assignment}"), 'p.id=pa.projectid AND pa.staffid =' . $staffid, array('staffid'));



        return $this->db->fetchAll($result);
    }

    function addprogressreport($arr, $staffid, $projectid, $d=null) {
        unset($arr['submit']);

        $arr['projectid'] = $projectid;
        $arr['staffid'] = $staffid;
        $arr['comment_date'] = date('Y-m-d');
        $tmp = explode('.',$d);
        $due_date = implode('-', $tmp);
        //die($due_date);
        $arr['due_date'] = date($due_date);//date('Y-m-d', $d);
        //die(var_dump($arr));
        
        $this->db->beginTransaction();
        try {


            if ($this->db->insert($this->tables->progress_report, $arr)) {
                $this->db->commit();
                
                
                return true;
            } else {
                $this->db->rollBack();
                return false;
            }
        } catch (Exception $e) {
            return $e;
        }
    }

    function getProgressReport($id, $staffid=null, $date=null) {
        $select = $this->db->select();
        $due_date= implode('-',explode('.',$date));
        
        if ($date != null) {
            if ($staffid != null) {
                
                $result = $select->from("{$this->tables->progress_report}")
                                ->where("projectid=$id")
                                ->where("staffid=$staffid")
                                ->where($this->db->quoteInto("due_date=?", $due_date));
            } else {
                $result = $select->from("{$this->tables->progress_report}")
                                ->where("projectid=$id")
                                //->where("staffid=$staffid")
                                ->where($this->db->quoteInto("due_date=?", $date));
            }
            return $this->db->fetchRow($result);
        } else {
            if ($staffid != null) {
                $result = $select->from("{$this->tables->progress_report}")
                                ->where("projectid=$id")
                                ->where("staffid=$staffid");
            } else {
                $result = $select->from("{$this->tables->progress_report}")
                                ->where("projectid=$id");
            }

            //die(print_r($this->db->fetchAll($result)));
            return $this->db->fetchAll($result);
        }
    }

    function updateProgress($staffid, $id, $data) {
        $due_date= date(implode('-',explode('.',$data['due_date'])));
        unset($data['due_date']);
      // die(var_dump($data['due_date']));
        
        $this->db->beginTransaction();

        unset($data['submit']);
        //die(var_dump($staffid));
        $query = $this->db->update("{$this->tables->progress_report}", $data, "projectid=$id AND due_date=$due_date");
        
        if ($query) {
            $this->db->commit();
            $this->statusUpdate($id);
            return true;
        } else {
            $this->db->rollBack();
            return false;
        }
    }

    function removeprogressreport($staffid, $id, $date) {
        
        $this->db->beginTransaction();
        $due_date= date(implode('-',explode('.',$date)));
        $query = $this->db->delete("{$this->tables->progress_report}", $this->db->quoteInto("due_date=?", $due_date));
        
        if ($query) {
            $this->db->commit();
            $this->statusUpdate($id);
            return true;
        } else {
            $this->db->rollBack();
            return false;
        }
    }

    function addEval($data) {
        unset($data['submit']);
        $this->db->beginTransaction();
        try {


            if ($this->db->insert($this->tables->eval, $data)) {
                $this->db->commit();
                return true;
            } else {
                $this->db->rollBack();
                return false;
            }
        } catch (Exception $e) {
            return $e;
        }
    }

    /*
     *  Project Evaluation
     */

    function addEvaluation($data, $id) {

        unset($data['submit']);
        $data['projectid'] = $id;

        if (!$this->getEvaluation($id)){

            $res = $this->db->insert($this->tables->eval, $data) or die("Failed inserting");
        } else {
            $res = $this->db->update("{$this->tables->eval}", $data, "projectid=" . $id);
        }

        $this->db->beginTransaction();
        try {


            if ($res) {
                $this->db->commit();
                return true;
            } else {
                $this->db->rollBack();
                return false;
            }
        } catch (Exception $e) {
            return $e;
        }
    }

    function getEvaluation($id) {

        $select = $this->db->select();
        $result = $select->from(array('e' => "{$this->tables->eval}"))
                        ->where($this->db->quoteInto("e.projectid=?", $id));


        return $this->db->fetchRow($result);

//        $res = $this->db->select()
//                        ->from('project_evaluation')
//                        ->where($this->db->quoteInto("projectid=?", $id));
//        return $this->db->fetchAll($res);
    }

 // Depretiated 
    function updateStatus($projectid,$tolcount,$count){
        
        $data['status'] = round(($count/$tolcount)*100);

        try{
            $res = $this->db->update("{$this->tables->eval}", $data, "projectid=". $projectid);
        }catch(Exception $e){

             $res = $this->db->insert($this->tables->project,$data);
        }



        $this->db->beginTransaction();
        try {

            if ($res) {
                $this->db->commit();
                return true;
            } else {
                $this->db->rollBack();
                return false;
            }
        } catch (Exception $e) {
            return $e;
        }
    }
    
    /*
     *  Project Status Calculate - New
     */
    
    function statusUpdate($projectid){
        //return true;
//        $sql="SELECT *,datediff(end_date,start_date) as Diff,count(*) as Count,round(datediff(end_date,start_date)/progress_cycle) as Total_Count,round((count(*)/round(datediff(end_date,start_date)/progress_cycle))*100) as Progress Status FROM project p LEFT JOIN user_project_status s 
//ON p.id=s.projectid GROUP BY p.id";
      //  $this->db->beginTransaction();
        $sql="SELECT p.id,round((count(*)/round(datediff(end_date,start_date)/progress_cycle))*100) as Progress_Status FROM project p LEFT JOIN user_project_status s 
ON p.id=s.projectid WHERE p.id={$projectid} GROUP BY p.id";
        //die($sql);
        $smnt = $this->db->query($sql);
        $result = $smnt->fetchAll();
       // die(print_r($result));
        //die($result);
        $sql="UPDATE project e SET status={$result[0]['Progress_Status']} WHERE e.id={$projectid}";
       // die($sql);
        $smnt = $this->db->query($sql);
        
            if ($smnt) {
         //       $this->db->commit();
                return true;
            } else {
         //       $this->db->rollBack();
                return false;
            }   
            
    }
    
}


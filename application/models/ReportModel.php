<?php

class Model_ReportModel extends Zend_Db_Table {

    protected $db;
    protected $tables;

    function __construct() {
        $this->db = Zend_Registry::get('db');
        $this->tables = Zend_Registry::get('tables');
    }

    /*
     *  Get a list of Employees both employed and terminated
     */

    function getStaffList($param) {
        $select = $this->db->select();
        //return 0;
        //die($param);
        if ($param == 'all') {
            $result = $select->from(array("u" => $this->tables->user))
                    ->join(array("pis" => $this->tables->profile_pis), "u.id=pis.staffid")
                    ->join(array("r" => $this->tables->recruit), "u.id=r.staffid")
                    ->joinLeft(array("d" => $this->tables->designation), "r.designation_id=d.id");
        } else {
            if ($param == 'past') {
                $whr = "r.date_of_termination IS NOT NULL";
            }
            if ($param == 'present') {
                $whr = "r.date_of_termination IS NULL";
            }
            $result = $select->from(array("u" => $this->tables->user))
                    ->join(array("pis" => $this->tables->profile_pis), "u.id=pis.staffid")
                    ->join(array("r" => $this->tables->recruit), "u.id=r.staffid")
                    ->joinLeft(array("d" => $this->tables->designation), "r.designation_id=d.id")
                    ->where($whr);
        }
        // die("as");//print_r($this->db->fetchAll($result)));
        return $this->db->fetchAll($result);
    }

    /*
     *  Get a list of staff leaves
     */

    function getLeaveReport($staffid, $start_date, $end_date, $status) {
        //return $staffid;
        $select = $this->db->select();
        if ($status == 'any') {
            if ($staffid == 'all') {

                $result = $select->from(array('ul' => "{$this->tables->user_leave}"))
                        //->where($this->db->quoteInto("status=?", $status))
                        ->where($this->db->quoteInto("ul." . $this->db->quoteIdentifier('from') . ">=?", $start_date))
                        ->where($this->db->quoteInto("ul.to<=?", $end_date));
            } else {
                // $whr = $this->db->quoteInto("ul.staffid=?", $staffid);
                $result = $select->from(array('ul' => "{$this->tables->user_leave}"))
                        //->where($this->db->quoteInto("status=?", $status))
                        ->where($this->db->quoteInto("ul.staffid=?", $staffid))
                        ->where($this->db->quoteInto("ul." . $this->db->quoteIdentifier('from') . ">=?", $start_date))
                        ->where($this->db->quoteInto("ul.to<=?", $end_date));
            }
        } else {

            if ($staffid == 'all') {
                $result = $select->from(array('ul' => "{$this->tables->user_leave}"))
                        ->where($this->db->quoteInto("ul.status=?", $status))
                        ->where($this->db->quoteInto("ul." . $this->db->quoteIdentifier('from') . ">=?", $start_date))
                        ->where($this->db->quoteInto("ul.to<=?", $end_date));
            } else {
                // $whr = $this->db->quoteInto("ul.staffid=?", $staffid);
                $result = $select->from(array('ul' => "{$this->tables->user_leave}"))
                        ->where($this->db->quoteInto("ul.status=?", $status))
                        ->where($this->db->quoteInto("ul.staffid=?", $staffid))
                        ->where($this->db->quoteInto("ul." . $this->db->quoteIdentifier('from') . ">=?", $start_date))
                        ->where($this->db->quoteInto("ul.to<=?", $end_date));
            }
            //return die($staffid);
        }

        //return die($status);

        return $this->db->fetchAll($result);
    }

    function getLeaveSummary($staffid=null, $status) {

        $select = $this->db->select();

        if ($status == 'any') {
            $result = $select->from(array('ul' => "{$this->tables->user_leave}"), array(
                        'ul.type as type', 'ul.year as year', 'SUM(ul.count) as total'
                    ))
                    //->where($this->db->quoteInto("status=?",$status))
                    ->where($this->db->quoteInto("ul.staffid=?", $staffid))
                    ->where($this->db->quoteInto("ul.status=?", 1))
                    ->group('ul.year')
                    ->group('ul.type');
        } else {

            if ($staffid != 'all') {

                $result = $select->from(array('ul' => "{$this->tables->user_leave}"), array(
                            'ul.type as type', 'ul.year as year', 'SUM(ul.count) as total'
                        ))
                        ->where($this->db->quoteInto("status=?", $status))
                        ->where($this->db->quoteInto("ul.staffid=?", $staffid))
                        ->where($this->db->quoteInto("ul.status=?", 1))
                        ->group('ul.year')
                        ->group('ul.type');
            }
        }
        
        $result = $this->db->fetchAll($result);
        $data = array();
        
        foreach ($result as $rec) {
            $data[$rec['year']][$rec['type']] = (int) $rec['count'];
        }
            
        $x = array();
        foreach ($data as $k => $d) {
            $x[] = array(array_values($d), "$k");
        }        
        echo "asd";die(print_r($x));
        return $x;
    }

    function overallLeave($from=null, $to=null) {

        if ($from == null && $to == null) {
            $sql = "SELECT year,type,count(*) as count FROM user_leave u GROUP BY u.type,u.year";
        } else {
            
        }

        $smnt = $this->db->query($sql);
        $result = $smnt->fetchAll();

        return $this->formatChart($result);
    }

    function getStaffLeaveOverview($staffid) {
        $sql = "SELECT year,type,count(*) as count FROM user_leave u WHERE staffid=" . $staffid . " GROUP BY u.type,u.year";
        //die($sql);
        $smnt = $this->db->query($sql);
        $result = $smnt->fetchAll();
        //die(printr($result));    
        //return $result;
        return $this->formatChart($result);
    }
    
    function formatChart($result){
        $data = array();
        $codes= array('A','C','M');
        foreach ($result as $rec) {
            foreach($codes as $code){
                $data[$rec['year']][$code] = 0;
            }
        }
        foreach ($result as $rec) {            
            $data[$rec['year']][$rec['type']] = (int) $rec['count'];
        }
        
        $x = array();
        foreach ($data as $k => $d) {
            $x[] = array(array_values($d), "$k");
        }
   
        //return array(array(array(1,2,3),'2010'),array(array(12,4,1),'2011'));
   
        return $x;       
    }

    function _overallLeave() {
        $sql = "SELECT year,type,count(*) as count FROM user_leave u GROUP BY u.type,u.year";
        $smnt = $this->db->query($sql);
        $result = $smnt->fetchAll();
        $str = '';
        $data = array();
        foreach ($result as $rec) {

            if (array_key_exists($rec['year'], $data)) {
                $data[$rec['year']] .= ';' . $rec['count'];
            } else {
                $data[$rec['year']] = $rec['year'] . ';' . $rec['count'];
            }
        }
        foreach ($data as $d) {
            $str .= $d . '\n';
        }
        return $str;
    }

    /*
     *  Get user Login information
     */

    function getLoginInfo($status='any',$type='all') {
        
        if($status != 'any'){
            $whr = " AND r.status={$status}";
        }else{
            $whr="";
        }
        if($type != 'all' ){
            $whr2 = " AND u.user_type='{$type}'";
        }else{
            $whr2="";
        }        
     
        $select = $this->db->select();

        $result = $select->from(array("u" => $this->tables->user))
                ->join(array("pis" => $this->tables->profile_pis), "u.id=pis.staffid")
                ->join(array("r" => $this->tables->recruit), "u.id=r.staffid {$whr} {$whr2}");
           

        return $this->db->fetchAll($result);
    }

    /*
     *  Get user type analysis
     */

    function getUserTypeAnalysis() {
        $select = $this->db->select();

        $result = $select->from(array("u" => $this->tables->user), array('COUNT(u.user_type) as count', 'u.user_type'))
                ->group('u.user_type');


        return $this->db->fetchAll($result);
    }

    function getUserTraining() {
        
    }

    /*
     *  Performance Reports
     */

    function getPerformanceByStaff($staffid) {

        // $sql = "SELECT a.staffid,a.rating,a.year,r.title FROM " . $this->tables->appraisal . " a,".$this->tables->rating." r WHERE a.staffid=" . (int) $staffid."AND a.rating=r.id";
//        $config = new Zend_Config_Ini(APPLICATION_PATH.'/configs/config.ini');die(print_r($config));
//        $ratings = $config->rating;

        $select = $this->db->select();

        $result = $select->from(array("u" => $this->tables->appraisal))
                ->join(array("r" => $this->tables->rating), "u.staffid={$staffid} AND u.rating=r.id")
                ->order('u.year desc');

        return $this->db->fetchAll($result);

//        return $result;
    }

    function getRatings() {
        $select = $this->db->select();

        $result = $select->from(array("u" => $this->tables->rating), array('title', 'id'));
        //->order('title ASC');
        return $this->db->fetchAll($result);

        //return $result;
    }

    /*
     *   Summary of ratings , for each year how many 1 raters , how many 2 raters etc ...   
     */

    function getRatingOverview() {

        $sql = "SELECT a.year,a.rating,count(a.rating) as count FROM appraisal a GROUP BY a.year,a.rating order by a.year DESC";
        $smnt = $this->db->query($sql);
        $result = $smnt->fetchAll();
        // print_r($result);echo "<hr/>";
        $data = array();
        $x = array();
        foreach ($result as $rec) {
            if (!key_exists($rec['year'], $data)) {
                $data[$rec['year']] = array($rec['year'], array($rec['rating'], (int) $rec['count']));
            } else {
                $temp = $data[$rec['year']];
                //array_push($temp, $rec['rating']);
                //array_push($temp, $rec['count']);
                array_push($temp, array($rec['rating'], (int) $rec['count']));
                $data[$rec['year']] = $temp;
            }
        }
        // All rating counts are Zero
            $rating = array();
            $rating["1"] = 0;
            $rating["2"] = 0;
            $rating["3"] = 0;
            $rating["4"] = 0;
            $rating["5"] = 0;

        foreach ($data as $key => $items) {
            $year = array_shift($data[$key]);


            foreach ($items as $k => $item) {
                if (is_array($item)) {
                    $rating["$item[0]"] = $item[1];
                }
            }



            array_push($x, array($rating, $year));
            array_push($data[$key], $year);
            //Rating count is set to Zero
            $rating["1"] = 0;
            $rating["2"] = 0;
            $rating["3"] = 0;
            $rating["4"] = 0;
            $rating["5"] = 0;
        }


        //print_r($x);
        //die();
        return $x;
    }
    
    
    function getAllTrainings(){
        
        $sql = "SELECT * FROM ".$this->tables->user_training." t,".$this->tables->profile_pis." pis WHERE
               t.staffid=pis.staffid";
        $smnt = $this->db->query($sql);
        $result = $smnt->fetchAll();
        //print_r($result);die();
        return $result;
    }
    
        function getAllTrainingsByStaff($staffid,$status,$start,$end){
            
        if($status == 'any'){
            $whr = '';
        }else{
            $whr = " AND t.status={$status}";
        }
        
        if($staffid == 'all'){
            $whr2 = '';
        }else{
            $whr2 = " AND t.staffid={$staffid}";
        }
        
        $sql = "SELECT * FROM ".$this->tables->user_training." t,".$this->tables->profile_pis." pis WHERE
               t.staffid=pis.staffid AND request_date > '{$start}' AND request_date < '{$end}'";
               $sql .= $whr . $whr2;
        
        //return $sql;

        $smnt = $this->db->query($sql);
        $result = $smnt->fetchAll();
        //print_r($result);die();
        
        return $result;
    }

    /*
     *  Staff Salary review
     */
    function getStaffSalaryReview($staffid){
        if($staffid !='all'){
           
            $sql ="SELECT * FROM ".$this->tables->user_pay." up,".$this->tables->profile_pis." pis WHERE 
               up.staffid=pis.staffid AND up.staffid=$staffid order by date ASC";
            
        }else{
            
            $sql ="SELECT *,MAX(date) FROM ".$this->tables->user_pay." up,".$this->tables->profile_pis." pis WHERE 
               up.staffid=pis.staffid AND up.active=1 GROUP BY up.staffid order by date ASC";
        }
//        $sql ="SELECT * FROM ".$this->tables->user_pay." up,".$this->tables->profile_pis." pis WHERE 
//               up.staffid=pis.staffid {$whr} order by date ASC";
        
        //return $sql;

        $smnt = $this->db->query($sql);
        $result = $smnt->fetchAll();
       // print_r($result);die();
        
        return $result;
    }
    
    /*
     *  Human Resource Cost for company analysis
     */
    function getHrCoc(){
        $sql ="SELECT YEAR(date) as year,ROUND(avg(salary),2) as coc FROM user_pay GROUP BY YEAR(date)";

        
        $smnt = $this->db->query($sql);
        $result = $smnt->fetchAll();
       // print_r($result);die();
        
        return $result;
    }
    
    /*
     *  Get Project details
     */
    function getProjectDetails($param){
        if($param['title']!=''){
            //$whr = "project.title LIKE '%{$param['title']}%'";

//            $sql = "SELECT * FROM project
//                WHERE title LIKE '%{$param['title']}%'";  
                
            $whr1="p.title LIKE '%{$param['title']}%'";
        }else{
            $whr1='';
        }
        
        if(isset($param['duration'])){

            if($param['duration']=3){
                 $cond=">=3";
            }             
            if($param['duation']=2){
                $cond="<=2";
            } 
            
//             $sql = "SELECT project.*,period_diff(date_format(end_date,'%Y%m'),date_format(start_date,'%Y%m')) as Months FROM project p 
//                     WHERE Months {$cond} ;";
             if($whr1!=''){
             $whr2=" AND period_diff(date_format(end_date,'%Y%m'),date_format(start_date,'%Y%m')) {$cond}";
             }else{
             $whr2=" period_diff(date_format(end_date,'%Y%m'),date_format(start_date,'%Y%m')) {$cond}";
             }
        }
        
        if($param['duration']==1){
            $whr2=" ";
        }
        
        
        if($param['status']==1){
//           $sql = "SELECT * FROM project p 
//                     WHERE p.end_date> NOW() ;";
     
            $whr3=" AND p.end_date< NOW()";
           
        }
        
        if($param['status']==0){
            $whr3="AND p.end_date > NOW()";
        }
        
        if($param['status']==2){
            $whr3="";
        }        
        
        
        $sql="SELECT * 
              FROM project p
              WHERE {$whr1} {$whr2} {$whr3}";       
        
        die($sql);
        $smnt = $this->db->query($sql);
        $result = $smnt->fetchAll();
        print_r($result);die();
        
        return $result;        
    }
    
    /*
     *  List of active Projects
     */
    
    function activeProjects(){
        $select = $this->db->select();
        
        $result = $select->from("{$this->tables->project}");
                   
        return $this->db->fetchAll($result);
    }
    
    function completeProjects(){
        $select = $this->db->select();
        
        $result = $select->from("{$this->tables->project}")
                         ->joinLeft(array("eval"=>$this->tables->eval),"eval.projectid=project.id")
                         ->where($this->db->quoteInto("status=?",100));
                   
        return $this->db->fetchAll($result);
    }
    /*
     *  List of ongoin projects
     */
    
    function ongoingProjects(){
        $select = $this->db->select();
        
        $result = $select->from("{$this->tables->project}")                         
                         ->where($this->db->quoteInto("status<?",100));
                   
        return $this->db->fetchAll($result);
    }
    
    function projectbyStaff($staffid){
        $select = $this->db->select();
        
        $result = $select->from(array("p"=>$this->tables->project))                         
                         ->joinLeft(array("a"=>$this->tables->project_assignment),"p.id=a.projectid")
                         ->where($this->db->quoteInto("staffid=?",$staffid));
                   
        return $this->db->fetchAll($result);        
    }
    
    
    function projectbyCycle($cycle){
        
        $select = $this->db->select();
        
        $result = $select->from(array("p"=>$this->tables->project))                                                  
                         ->where($this->db->quoteInto("progress_cycle=?",$cycle));
                   
        return $this->db->fetchAll($result);        
    }  
    
    /*
     *  active projects completion comparison
     */
    function projectCompletion(){
        
        $select = $this->db->select();
        
        $result = $select->from(array("p"=>$this->tables->project))
                         ->where($this->db->quoteInto("p.status<?",100))
                         ->order("p.status DESC"); 
                         
                   
        return $this->db->fetchAll($result); 
    }
   /*
    *  Delayed Projects
    */
    
    function projectsDealyed(){
        
        $select = $this->db->select();
        
        $result = $select->from(array("p"=>$this->tables->project))
                         ->where($this->db->quoteInto("p.end_date<?","NOW()"))
                         ->where($this->db->quoteInto("p.status<?",100));                                   
                   
        return $this->db->fetchAll($result); 
    }
    
}


?>
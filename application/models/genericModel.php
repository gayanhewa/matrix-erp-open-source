<?php

class Model_GenericModel extends Zend_Db_Table
{
    protected $db;
    protected $tables;


    function Model_UserModel()
    {
        $this->db = Zend_Registry::get('db');
        $this->tables = Zend_Registry::get('tables');
    }

    function getRecordById($table,$id)
    {
        $select = $this->db->select();

        $result = $select->from($table)
                         ->where($table,$this->db->qoteInto("id=?",$id));

         return $this->db->fetchRow($result);
    }
}

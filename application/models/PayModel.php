<?php

class Model_PayModel extends Zend_Db_Table{

    protected $db;
    protected $tables;

    function __construct() {
        $this->db = Zend_Registry::get('db');
        $this->tables = Zend_Registry::get('tables');
    }

    function addPayscale($data) {


        if ($this->db->insert($this->tables->user_pay, $data)
        ) {

            return true;
        } else {

            return false;
        }
    }

    function updatePayscale($data) {
        $this->db->beginTransaction();

        if ($this->db->insert($this->tables->user_pay, $data)
        ) {
            $this->db->commit();

            return $data;
        } else {
            $this->db->rollBack();
            return false;
        }
    }

    function getUserList() {

        $select = $this->db->select();

        $result = $select->from(array('pis' => $this->tables->profile_pis))
                        ->joinLeft(array('p' => "{$this->tables->user_pay}"), "p.staffid=pis.staffid AND p.active=1");



        //return $this->db->fetchAll($result);


        return $this->db->fetchAll($result);
    }

    function getUser($id) {
        $select = $this->db->select();

        $result = $select->from(array('p' => "{$this->tables->user_pay}"))
                        ->join(array('pis' => $this->tables->profile_pis), "p.staffid=pis.staffid AND p.staffid={$id}",array('pis.full_name'))
                        ->order('date');



        //return $this->db->fetchAll($result);


        return $this->db->fetchAll($result);
    }

    function addPayhike($data, $id) {
      
        $data['staffid'] = $id;
        $data['date'] = $data['datepicker'];
        $data['active'] = 1;
        unset($data['datepicker']);
        unset($data['submit']);
        //return $data;
//        $curr_sal =(float) $this->getCurrentSal($id);
//        die(var_dump(($data['salary']-$curr_sal)/$curr_sal));
        $curr_sal =(float) $this->getCurrentSal($id);
        $data['increment'] = round((($data['salary']-$curr_sal)/$curr_sal)*100);
          $this->deactivatePay($id);
        $this->db->beginTransaction();
        if ($this->db->insert($this->tables->user_pay, $data)) {
            $this->db->commit();
            return true;
        } else {
            $this->db->rollBack();
            return false;
        }
    }
    
    function getCurrentSal($staffid){
        $select = $this->db->select();

        $result = $select->from(array('pis' => $this->tables->user_pay),array('salary'))
                         ->where("pis.active=1")
                         ->where("pis.staffid={$staffid}");
                  


        return $this->db->fetchOne($result);        
    }
    function deactivatePay($id) {
        $this->db->beginTransaction();
        if ($this->db->update($this->tables->user_pay, array('active' => 0), "staffid={$id}")) {
            $this->db->commit();
            return true;
        } else {
            $this->db->rollBack();
            return false;
        }
    }

    function removePay($id) {
//        return $id;
        $this->db->beginTransaction();

        //if ($this->db->delete($this->tables->user_pay,$this->db->quoteInto("staffid=?", $id))) {
        if ($this->db->delete("{$this->tables->user_pay}", $this->db->quoteInto("id=?", $id))) {
            $this->db->commit();
            return true;
        } else {
            $this->db->rollBack();
            return false;
        }
    }


}

?>
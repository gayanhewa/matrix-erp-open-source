<?php

class Model_ObjectivesModel
{
    protected $db;
    protected $tables;

    function Model_ObjectivesModel()
    {
        $this->db = Zend_Registry::get('db');
        $this->tables = Zend_Registry::get('tables');
    }

  function addObjective($data){
 /*       $role['staffid'] = $data['staffid'];
        $role['title'] = $data['title'];
        $role['desc'] = $data['desc'];
   */

      unset($data['submit']);

        $this->db->beginTransaction();


        if($this->db->insert("{$this->tables->objectives}",$data))
         {
            $this->db->commit();
            return true;
          }else{
            $this->db->rollBack();
            return false;
           }
    }

  function viewobjectives($staffid,$year=null){
        $select = $this->db->select();
        if($year!=null)
        {
        $result = $select->from(array('o'=>"{$this->tables->objectives}"))
                        ->where($this->db->quoteInto('o.staffid =?',$staffid))
                        ->where($this->db->quoteInto('o.year=?',$year));
        }else{
        $result = $select->from(array('o'=>"{$this->tables->objectives}"))
                        ->where($this->db->quoteInto('o.staffid =?',$staffid));
        }
        return $this->db->fetchAll($result);
   }

   function getmycomments($staffid,$year=null,$id=null)
   {
        $select = $this->db->select();

        if($id!=null){

        $result = $select->from(array('o'=>"{$this->tables->objective_comment}"))
                        ->where($this->db->quoteInto('o.staffid =?',$staffid))
                        ->where($this->db->quoteInto('o.objectiveid =?',$id))
                         ->where($this->db->quoteInto('o.year =?',$year));
        }else{

        $result = $select->from(array('o'=>"{$this->tables->objective_comment}"))
                        ->where($this->db->quoteInto('o.staffid =?',$staffid));;
        }
        return $this->db->fetchAll($result);
   }

   function addmycomments($data,$staffid,$objectiveid,$year,$overall=null)
   {
        unset($data['submit']);
        $data['staffid'] = $staffid;
        $data['objectiveid'] = $objectiveid;
        $data['year'] = $year;
        $this->db->beginTransaction();


        if($this->db->insert("{$this->tables->objective_comment}",$data))
         {
            $this->db->commit();
            return true;
          }else{
            $this->db->rollBack();
            return false;
          }
   }

   function updateComment($id,$arr,$staffid)
   {
        $this->db->beginTransaction();
        unset($arr['submit']);
        if($this->db->update("{$this->tables->objective_comment}",$arr,"objectiveid={$id} AND staffid={$staffid}"))
        {
            $this->db->commit();
            return true;
        }
        else
        {
            $this->db->rollBack();
            return false;
        }
   }

      function removecomment($staffid,$id)
   {
      $this->db->beginTransaction();


       //return $this->db->delete("{$this->tables->user_leave}",$this->db->quoteInto("id=?",$id));

       if($this->db->delete("{$this->tables->objective_comment}","objectiveid={$id} AND staffid={$staffid}"))
        {
            $this->db->commit();
            return true;
        }
        else
        {
            $this->db->rollBack();
            return false;
        }


   }


      function removeobjective($id)
   {
      $this->db->beginTransaction();


       //return $this->db->delete("{$this->tables->user_leave}",$this->db->quoteInto("id=?",$id));

       if($this->db->delete("{$this->tables->objectives}",$this->db->quoteInto("id=?",$id)))
        {
            $this->db->commit();
            return true;
        }
        else
        {
            $this->db->rollBack();
            return false;
        }


   }

   function getObjectiveById($id)
   {
        $select = $this->db->select();

        $result = $select->from(array('o'=>"{$this->tables->objectives}"))
                        ->where($this->db->quoteInto('o.id =?',$id));


        return $this->db->fetchRow($result);
   }

   function updateObjective($id,$arr)
   {

       $this->db->beginTransaction();
        unset($arr['submit']);
        $res = $this->db->update("{$this->tables->objectives}",$arr,$this->db->quoteInto("id=?",$id));
        if($res)
        {
            $this->db->commit();
            return true;
        }
        else
        {
            $this->db->rollBack();
            return false;
        }
   }

   function getObjectives($id)
   {
       $select = $this->db->select();
       $result = $select->from(array('obj'=>"{$this->tables->objectives}"))
                        ->joinLeft(array('obj_c'=>"{$this->tables->objective_comment}"),"obj.id=obj_c.objectiveid",array('comment','self_rating','year'))
                        ->where($this->db->quoteInto("obj.staffid=?",$id));

   
       return $this->db->fetchAll($result);
   }
   function addManagersComment($id,$data,$manager_staffid)
   {
      unset($data['submit']);
      unset($data['id']);
        $data['staffid']=$id;
        $data['manager_staffid']= $manager_staffid;
        
        $data['year']=date('Y');
        //return $data;
        $this->db->beginTransaction();


        if($this->db->insert("{$this->tables->appraisal}",$data))
         {
            $this->db->commit();
            return true;
          }else{
            $this->db->rollBack();
            return false;
           }
   }
   function getManagersComment($id)
   {
       $select = $this->db->select();
       $result = $select->from(array('obj'=>"{$this->tables->appraisal}"))
                        ->where($this->db->quoteInto("obj.staffid=?",$id));


       return $this->db->fetchAll($result);
   }

   function removeManagerComment($id)
   {
      $this->db->beginTransaction();


       //return $this->db->delete("{$this->tables->user_leave}",$this->db->quoteInto("id=?",$id));

       if($this->db->delete("{$this->tables->appraisal}",$this->db->quoteInto("id=?",$id)))
        {
            $this->db->commit();
            return true;
        }
        else
        {
            $this->db->rollBack();
            return false;
        }
   }
}


<?php

class Model_Designation
{

    protected $db;
    protected $tables;

    function Model_Designation()
    {
        $this->db = Zend_Registry::get('db');
        $this->tables = Zend_Registry::get('tables');
    }

    function getAll(){
        $select = $this->db->select();
        $result = $select ->from($this->tables->designation);
        return $this->db->fetchAll($result);
    }

    function addDesignation(){

    }

    function updateDesignation(){
        
    }

}


<?php

class Model_JobdescriptionModel
{
    protected $db;
    protected $tables;

    function Model_JobdescriptionModel()
    {
        $this->db = Zend_Registry::get('db');
        $this->tables = Zend_Registry::get('tables');
    }
    function getList(){
        $select = $this->db->select();
        $result = $select->from("{$this->tables->jobdesc}");
        
        return $this->db->fetchAll($result);
            
    }

    function getUserRole($staffid){
        $select = $this->db->select();
        $result = $select->from(array('jd'=>"{$this->tables->jobdesc}"))
                         ->join(array('ra'=>"{$this->tables->role_assignment}"),"jd.id=ra.jobid AND ra.staffid=$staffid");
       
        return $this->db->fetchRow($result);

    }

    function addRole($data,$staffid){
        unset($data['submit']);

        $this->db->beginTransaction();

        $res = $this->db->insert("{$this->tables->jobdesc}",$data);
        $job_id = $this->db->lastInsertId();
        if ($res) {
            if ($this->assignRole($staffid, $job_id)) {
                $this->db->commit();
                return $job_id;
            } else {
                $this->db->rollBack();
                return false;
            }
        } else {
            $this->db->rollBack();
            return false;
        }
    }

    function assignRole($staffid, $job_id) {

        //$this->db->beginTransaction();

        if ($this->db->insert("{$this->tables->role_assignment}", array('staffid' => $staffid, 'jobid' => $job_id))) {

            return true;
        } else {

            return false;
        }
    }


    function updateRole($data,$staffid,$jobid) {

        unset($data['submit']);

        $this->db->beginTransaction();

        if ($this->db->update("{$this->tables->jobdesc}",$data,$this->db->quoteInto("id=?",$jobid)))
        {
            $this->db->commit();
            return true;
        }else{
            $this->db->rollBack();
            return false;
        }
    }


    }
    




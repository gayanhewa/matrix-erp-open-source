<?php

class Model_UserModel extends Zend_Db_Table {

    protected $db;
    protected $tables;

    function __construct(){
        $this->db = Zend_Registry::get('db');
        $this->tables = Zend_Registry::get('tables');
    }

    function getUserType($username) {
        $select = $this->db->select();

        $result = $select->from(array('u' => "{$this->tables->user}"))
                        ->where("u.username = ?", $username);

        $this->setUserType($this->db->fetchRow($result));
        return $this->db->fetchRow($result);
    }

    function setUserType($user) {
        $auth = Zend_Auth::getInstance();
        $auth->getStorage()
                ->write($user);
    }

    function addProfile($profile) {
        require_once('password.php');
        $password = make_daily_password();
       
        $tables = Zend_Registry::get('tables');
       // die(var_dump($profile));
      //  print_r($profile);die();
        $data['email'] = $profile['email'];
        $data['password'] = md5($password+$tables->salt);
        $pis = array(
            'id' => '',
            'staffid' => $profile['staffid'],
            'nic' => $profile['nic'],
        //    'date_of_birth'=>NULL,
        //    'nationality'=>NULL,
        //    'full_ name'=>$profile['full_name'],
        //    'maritial_status'=>NULL
        );
        // $this->db->insert($this->tables->profile_pis, $pis);
       // die(var_dump($pis));
        $email = array(
            'id' => '',
            'address' => '',
            'email' => $profile['email'],
            'mobile_phone' => '',
            'residence_phone' => '',
            'staffid' => $profile['staffid']
        );
        $login = array(
            'id' => $profile['staffid'],
            'username' => $profile['email'],
            'password' => md5($password . $this->tables->salt),
            'user_type' => $profile['user_type']
        );
 
 
        
        $date = new Zend_Date();
        $date->set($profile['datepicker'],'YYYY-MM-dd');
        //echo $date->getDate();
        //die();
        $date->addMonth($profile['probation']) or die("Failed");
//        echo $date->getDate();
//        die();
        
        $recruit = array(
            'id' => '',
            'staffid' => $profile['staffid'],
            'designation_id' => $profile['designation'],
            'date_of_hire' => $profile['datepicker'],
            'confirmation_date'=>date('Y-m-d',$date->get()),
            'date_of_termination' => NULL,
            'status' => 1,
            'probation'=>$profile['probation'],
            
        );

        $pay = array(
            'id'=>'',
            'staffid'=> $profile['staffid'],
            'salary'=> $profile['salary'],
            'date'=>$profile['datepicker'],
            'increment'=>''
        );
       require_once 'PayModel.php';
        $model= new Model_PayModel();
        
        $this->db->beginTransaction();

       
        if ($this->db->insert($this->tables->profile_pis, $pis) &&
                $this->db->insert($this->tables->user, $login) &&
                $this->db->insert($this->tables->profile_contact, $email) &&
                $this->db->insert($this->tables->recruit, $recruit)&&
                $model->addPayscale($pay)
        ) {
                
            $this->db->commit();        
            return $data;
        } else {
 
            $this->db->rollBack();
            return false;
        }
    }

   

    function terminateUser($id, $s=null) {
        $this->db->beginTransaction();

        if ($s == null) {
            $res = $this->db->update("{$this->tables->recruit}",
                            array('status' => 0, 'date_of_termination' => date('Y-m-d')),
                            $this->db->quoteInto('staffid=?', $id));
        } else {
            $res = $this->db->update("{$this->tables->recruit}",
                            array('status' => 1, 'date_of_termination' => null),
                            $this->db->quoteInto('staffid=?', $id));
        }

        if ($res) {
            $this->db->commit();
            return true;
        } else {
            $this->db->rollBack();
            return false;
        }
    }

    function getUserList() {
        $select = $this->db->select();

        $result = $select->from(array('pis' => "{$this->tables->profile_pis}"))
                        ->join(array('r' => "{$this->tables->recruit}"), 'pis.staffid = r.staffid ')
                        ->join(array('d' => "{$this->tables->designation}"), 'r.designation_id = d.id');


        return $this->db->fetchAll($result);
    }

    function getUserInfo($staffid = null, $type = null, $id=null) {
        $select = $this->db->select();

        if ($id != null) {
            $whr = $this->db->quoteInto("id=?", $id);
        } else {
            $whr = $this->db->quoteInto("staffid=?", $staffid);
        }

        switch ($type) {
            case 'pis':
                $result = $select->from(array('pis' => "{$this->tables->profile_pis}"))
                                ->where($whr);
                return $this->db->fetchRow($result);
                break;
            case 'contact':

                $result = $select->from(array('pis' => "{$this->tables->profile_contact}"))
                                ->where($whr);
                //->where($this->db->quoteInto("pis.staffid =?",$param));

                if ($id != null) {
                    return $this->db->fetchRow($result);
                }

                return $this->db->fetchAll($result);

                break;
            case 'dependents':
                $result = $select->from(array('pis' => "{$this->tables->profile_dependents}"))
                                ->where($whr);
                //->where($this->db->quoteInto("pis.staffid =?",$param));
                return $this->db->fetchAll($result);
                break;
            case 'e_contact':
                $result = $select->from(array('pis' => "{$this->tables->profile_e_contact}"))
                                ->where($whr);
                //->where($this->db->quoteInto("pis.staffid =?",$param));
                return $this->db->fetchAll($result);
                break;
            case 'qual':
                $result = $select->from(array('pis' => "{$this->tables->profile_qual}"))
                                ->where($whr);
                // ->where($this->db->quoteInto("pis.staffid =?",$param));
                return $this->db->fetchAll($result);
                break;
        }
    }

    function addUserInfo($staffid, $arr, $type) {


        $arr['staffid'] = (int) $staffid;

        $this->db->beginTransaction();

        switch ($type) {

            case 'contact':
                unset($arr['submit']);
                unset($arr['id']);


                $result = $this->db->insert(
                                "{$this->tables->profile_contact}",
                                $arr
                );

                if ($result) {
                    $this->db->commit();
                    return true;
                } else {
                    $this->db->rollBack();
                    return false;
                }
                break;
            case 'dependents':
                unset($arr['submit']);
                $result = $this->db->insert(
                                "{$this->tables->profile_dependents}",
                                $arr
                );

                if ($result) {
                    $this->db->commit();
                    return true;
                } else {
                    $this->db->rollBack();
                    return false;
                }
                break;
            case 'e_contact':

                unset($arr['submit']);

                $arr['staffid'] = (int) $staffid;

                $result = $this->db->insert(
                                "{$this->tables->profile_e_contact}",
                                $arr
                );

                if ($result) {
                    $this->db->commit();
                    return true;
                } else {
                    $this->db->rollBack();
                    return false;
                }
                break;
            case 'qual':
                unset($arr['submit']);
                $result = $this->db->insert(
                                "{$this->tables->profile_qual}",
                                $arr
                );

                if ($result) {
                    $this->db->commit();
                    return true;
                } else {
                    $this->db->rollBack();
                    return false;
                }
                break;
        }
    }

    function updateUserInfo($staffid, $arr, $type, $id=null) {


        if ($id != null) {
            $whr = $this->db->quoteInto("id=?", $id);
        } else {
            $whr = $this->db->quoteInto("staffid=?", $staffid);
        }


        $this->db->beginTransaction();

        switch ($type) {
            case 'pis':
                unset($arr['id']);
                unset($arr['staffid']);
                unset($arr['submit']);
                $result = $this->db->update(
                                "{$this->tables->profile_pis}",
                                $arr,
                                $this->db->quoteInto("staffid=?", $staffid)
                );

                if ($result) {
                    $this->db->commit();
                    return true;
                } else {
                    $this->db->rollBack();
                    return false;
                }
                break;
            case 'contact':
                unset($arr['id']);
                unset($arr['staffid']);
                unset($arr['submit']);
                $result = $this->db->update(
                                "{$this->tables->profile_contact}",
                                $arr,
                                $whr
                );

                if ($result) {
                    $this->db->commit();
                    return true;
                } else {
                    $this->db->rollBack();
                    return false;
                }
                break;
            case 'dependents':
                unset($arr['id']);
                unset($arr['staffid']);
                unset($arr['submit']);
                $result = $this->db->update(
                                "{$this->tables->profile_dependents}",
                                $arr,
                                $whr
                );

                if ($result) {
                    $this->db->commit();
                    return true;
                } else {
                    $this->db->rollBack();
                    return false;
                }
                break;
            case 'e_contact':
                unset($arr['id']);
                unset($arr['staffid']);
                unset($arr['submit']);
                $result = $this->db->update(
                                "{$this->tables->profile_e_contact}",
                                $arr,
                                $whr
                );

                if ($result) {
                    $this->db->commit();
                    return true;
                } else {
                    $this->db->rollBack();
                    return false;
                }
                break;
            case 'qual':
                unset($arr['id']);
                unset($arr['staffid']);
                unset($arr['submit']);
                $result = $this->db->update(
                                "{$this->tables->profile_qual}",
                                $arr,
                                $whr
                );

                if ($result) {
                    $this->db->commit();
                    return true;
                } else {
                    $this->db->rollBack();
                    return false;
                }
                break;
        }
    }

    function requestLeave($arr, $staffid) {
        //return false;
        $date = new Zend_Date();
        $date_format = 'YYYY-MM-dd';

        unset($arr['submit']);
        $arr['staffid'] = $staffid;
        $arr['manager_staffid'] = null; //$staffid; //$this->getManagerId($staffid);
        $arr['request_date'] = $date->toString($date_format);
        $arr['year']= date("Y");

                          //echo "<pre>";print_r($this->getLeaveCount($staffid));die();


        //print $days
        //return $arr;

        $this->db->beginTransaction();

        if ($this->db->insert("{$this->tables->user_leave}", $arr)) {
            $this->db->commit();
            return true;
        } else {
            $this->db->rollBack();
            return false;
        }
    }

    function getManagerId($staffid) {
        //Find out the immediate line manager of the given staff id

        $select = $this - db();

        $result = $select->from("{$this->tables->user_matrix}", 'manager_staffid')
                        ->where($this->db->quoteInto("staffid = ?", $staffid));

        return $this->db->fetchRow($result);
    }

    function getLeaveReport($staffid=null, $id = null) {
        $select = $this->db->select();

        //If $staffid = null then return a leave report of all staff. if id has an value get that
        // record
        if ($staffid == null && $id == null) {

            $result = $select->from("{$this->tables->user_leave}");
        }

        if ($staffid != null) {
            if ($id == null) {
                $result = $select->from("{$this->tables->user_leave}")
                                ->where($this->db->quoteInto("staffid =?", $staffid))
                                ->where($this->db->quoteInto("status =?", 0));
            }

            if ($id != null) {
                $result = $select->from("{$this->tables->user_leave}", array('staffid', 'type', 'from', 'to', 'reason', 'status'))
                                ->where($this->db->quoteInto("id =?", $id))
                                ->where($this->db->quoteInto("status =?", 0));
                ;

                return $this->db->fetchRow($result);
            }
        }

        if ($staffid == null && $id != null) {
            $result = $select->from("{$this->tables->user_leave}", array('staffid', 'type', 'from', 'to', 'reason', 'status'))
                            ->where($this->db->quoteInto("id =?", $id))
                            ->where($this->db->quoteInto("status =?", 0));

            return $this->db->fetchRow($result);
        }
        return $this->db->fetchAll($result);
    }

    function getApprovedLeave($staffid=null) {
        $select = $this->db->select();

        if ($staffid != null) {
            $result = $select->from("{$this->tables->user_leave}")
                            ->where($this->db->quoteInto("status =?", 1))
                            ->where($this->db->quoteInto("staffid =?", $staffid));
        }
        if ($staffid == null) {
            $result = $select->from("{$this->tables->user_leave}")
                            ->where($this->db->quoteInto("status =?", 1));
        }


        return $this->db->fetchAll($result);
    }

    function upadateLeave($id, $arr) {
        $this->db->beginTransaction();
        unset($arr['submit']);
        if ($this->db->update("{$this->tables->user_leave}", $arr, $this->db->quoteInto("id =?", $id))) {
            $this->db->commit();
            return true;
        } else {
            $this->db->rollBack();
            return false;
        }
    }

    function removeLeave($id) {
        $this->db->beginTransaction();
        //return $id;
        if ($this->db->delete("{$this->tables->user_leave}", $this->db->quoteInto("id=?", $id))) {
            $this->db->commit();
            return true;
        } else {
            $this->db->rollBack();
            return false;
        }
    }

    function getTrainingReport($staffid=null, $id=null) {
        $select = $this->db->select();

        //If $staffid = null then return a leave report of all staff. if id has an value get that

        if ($staffid == null && $id == null) {

            $result = $select->from("{$this->tables->user_training}");
        }

        if ($staffid != null) {

            if (($id == null)) {
                $result = $select->from(array('t' => "{$this->tables->user_training}"))
                                ->where($this->db->quoteInto("t.staffid =?", $staffid));
                return $this->db->fetchAll($result);
            }

            if (($id != null)) {
                $result = $select->from("{$this->tables->user_training}")
                                ->where($this->db->quoteInto("id =?", $id));

                return $this->db->fetchRow($result);
            }
        }

        return $this->db->fetchAll($result);
    }

    function getApprovedTrainings() {
        $select = $this->db->select();
        $result = $select->from("{$this->tables->user_training}")
                        ->where($this->db->quoteInto("status=?", 1));

        return $this->db->fetchAll($result);
    }

    function getPendingTrainings() {
        $select = $this->db->select();
        $result = $select->from("{$this->tables->user_training}")
                        ->where($this->db->quoteInto("status=?", 0));

        return $this->db->fetchAll($result);
    }

    function requestTraining($arr, $staffid) {
        //return false;
        $date = new Zend_Date();
        $date_format = 'YYYY-MM-dd';

        unset($arr['submit']);
        $arr['staffid'] = $staffid;
        $arr['manager_staffid'] = $staffid;
        $arr['request_date'] = $date->toString($date_format);
        // return $arr;

        $this->db->beginTransaction();

        if ($this->db->insert("{$this->tables->user_training}", $arr)) {
            $this->db->commit();
            return true;
        } else {
            $this->db->rollBack();
            return false;
        }
    }

    function removeTraining($staffid, $id) {
        $this->db->beginTransaction();


        //return $this->db->delete("{$this->tables->user_leave}",$this->db->quoteInto("id=?",$id));

        if ($this->db->delete("{$this->tables->user_training}", $this->db->quoteInto("id=?", $id))) {
            $this->db->commit();
            return true;
        } else {
            $this->db->rollBack();
            return false;
        }
    }

    function upadateTraining($id, $arr) {
        $this->db->beginTransaction();
        unset($arr['submit']);
        if ($this->db->update("{$this->tables->user_training}", $arr, $this->db->quoteInto("id =?", $id))) {
            $this->db->commit();
            return true;
        } else {
            $this->db->rollBack();
            return false;
        }
    }

    function approveTraining($id, $staffid) {
        $this->db->beginTransaction();

        if ($this->db->update("{$this->tables->user_training}", array('status' => '1', 'approve_date' => date('Y-m-d'), 'manager_staffid' => $staffid), $this->db->quoteInto("id =?", $id))) {
            $this->db->commit();
            return true;
        } else {
            $this->db->rollBack();
            return false;
        }
    }

    function rejectTraining($id, $staffid) {
        $this->db->beginTransaction();

        if ($this->db->update("{$this->tables->user_training}", array('status' => '0', 'approve_date' => null, 'manager_staffid' => $staffid), $this->db->quoteInto("id =?", $id))) {
            $this->db->commit();
            return true;
        } else {
            $this->db->rollBack();
            return false;
        }
    }

    function getPendingLeaveReport() {
        $select = $this->db->select();
        $result = $select->from("{$this->tables->user_leave}")
                        ->where($this->db->quoteInto("status=?", 0));

        return $this->db->fetchAll($result);
    }

    function approveLeave($id, $staffid) {
        $this->db->beginTransaction();

        if ($this->db->update("{$this->tables->user_leave}", array('status' => '1', 'approve_date' => date('Y-m-d'), 'manager_staffid' => $staffid), $this->db->quoteInto("id =?", $id))) {
            $this->db->commit();
            return true;
        } else {
            $this->db->rollBack();
            return false;
        }
    }

    function rejectleave($id, $staffid) {
        $this->db->beginTransaction();

        if ($this->db->update("{$this->tables->user_leave}", array('status' => '0', 'approve_date' => null, 'manager_staffid' => $staffid), $this->db->quoteInto("id =?", $id))) {
            $this->db->commit();
            return true;
        } else {
            $this->db->rollBack();
            return false;
        }
    }

    function changePassword($formvals,$staffid) {

        if ($formvals['newpass1'] === $formvals['newpass2']) {
  
            $this->db->beginTransaction();

            if($this->db->update("{$this->tables->user}",array('password'=>$formvals['newpass1']), $this->db->quoteInto("id =?", $staffid))){
                $this->db->commit();
                return true;
            }else{
                $this->db->rollBack();
                return false;
            }
        }else{
            return false;
        }

    }




}


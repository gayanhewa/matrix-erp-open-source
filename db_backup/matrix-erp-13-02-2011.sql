-- phpMyAdmin SQL Dump
-- version 3.3.7deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 13, 2011 at 11:52 PM
-- Server version: 5.1.49
-- PHP Version: 5.3.3-1ubuntu9.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `matrixerp`
--

-- --------------------------------------------------------

--
-- Table structure for table `appraisal`
--

CREATE TABLE IF NOT EXISTS `appraisal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staffid` int(11) NOT NULL,
  `my_comment` text COLLATE utf8_unicode_ci NOT NULL,
  `managers_comment` text COLLATE utf8_unicode_ci NOT NULL,
  `rating` int(1) NOT NULL,
  `manager_staffid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `appraisal`
--


-- --------------------------------------------------------

--
-- Table structure for table `assignment`
--

CREATE TABLE IF NOT EXISTS `assignment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_nic` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `manager_nic` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `assignment`
--


-- --------------------------------------------------------

--
-- Table structure for table `designation`
--

CREATE TABLE IF NOT EXISTS `designation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `Desc` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `Basic` int(64) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Title` (`Title`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `designation`
--

INSERT INTO `designation` (`id`, `Title`, `Desc`, `Basic`) VALUES
(1, 'Lecturer', 'Conduct Lectures', 25000),
(2, 'Assistant Lecturer', 'Conduct Lectures', 20000);

-- --------------------------------------------------------

--
-- Table structure for table `jobdescription`
--

CREATE TABLE IF NOT EXISTS `jobdescription` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `job_role` text CHARACTER SET latin1 NOT NULL,
  `Title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `jobdescription`
--


-- --------------------------------------------------------

--
-- Table structure for table `job_role_assignment`
--

CREATE TABLE IF NOT EXISTS `job_role_assignment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_nic` int(11) NOT NULL,
  `job_spec_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `employee_nic` (`employee_nic`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `job_role_assignment`
--


-- --------------------------------------------------------

--
-- Table structure for table `objectives`
--

CREATE TABLE IF NOT EXISTS `objectives` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staffid` int(11) NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `desc` text COLLATE utf8_unicode_ci NOT NULL,
  `year` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `objectives`
--

INSERT INTO `objectives` (`id`, `staffid`, `title`, `desc`, `year`) VALUES
(1, 111, 'Financial', 'Good', 2009),
(2, 111, 'Service', 'Excellent', 2011),
(3, 111, 'New Objective', 'This is it<br>', 2011);

-- --------------------------------------------------------

--
-- Table structure for table `objective_comments`
--

CREATE TABLE IF NOT EXISTS `objective_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staffid` int(11) NOT NULL,
  `objectiveid` int(11) NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `self_rating` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `year` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `objective_comments`
--

INSERT INTO `objective_comments` (`id`, `staffid`, `objectiveid`, `comment`, `self_rating`, `year`) VALUES
(2, 111, 3, 'Anoh', 'red', 2011);

-- --------------------------------------------------------

--
-- Table structure for table `profile_academic_qual`
--

CREATE TABLE IF NOT EXISTS `profile_academic_qual` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `institution` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `staffid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `profile_academic_qual`
--

INSERT INTO `profile_academic_qual` (`id`, `type`, `institution`, `staffid`) VALUES
(1, 'dfsdf', 'wwersdfsdfsdffsf', 7654),
(2, 'sdfsdf', 'sdfsdf', 7654);

-- --------------------------------------------------------

--
-- Table structure for table `profile_contact`
--

CREATE TABLE IF NOT EXISTS `profile_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staffid` int(11) NOT NULL,
  `email` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `mobile_phone` int(11) DEFAULT NULL,
  `residence_phone` int(11) DEFAULT NULL,
  `address` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=27 ;

--
-- Dumping data for table `profile_contact`
--

INSERT INTO `profile_contact` (`id`, `staffid`, `email`, `mobile_phone`, `residence_phone`, `address`) VALUES
(1, 999, 'fsdfsfd', 123, 2332, '323'),
(23, 7654, 'aaa', 112321, 123123, 'dfghgfh'),
(25, 7654, 'AAAAAA', 12123324, 11111, 'AAA BBBB CCC'),
(26, 7654, 'aaa', 12123324, 11111, 'AAA BBBB CCC');

-- --------------------------------------------------------

--
-- Table structure for table `profile_dependents`
--

CREATE TABLE IF NOT EXISTS `profile_dependents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `relationship` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `nic` int(11) DEFAULT NULL,
  `staffid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `profile_dependents`
--

INSERT INTO `profile_dependents` (`id`, `full_name`, `relationship`, `date_of_birth`, `nic`, `staffid`) VALUES
(1, 'sdfdfg', 'dfgdfg', '2011-01-25', 5245345, 0),
(2, 'asdwer', 'wer', '2011-01-19', 0, 7654);

-- --------------------------------------------------------

--
-- Table structure for table `profile_emergency_contact`
--

CREATE TABLE IF NOT EXISTS `profile_emergency_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `relationship` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `contact_no` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `staffid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `profile_emergency_contact`
--

INSERT INTO `profile_emergency_contact` (`id`, `full_name`, `relationship`, `contact_no`, `staffid`) VALUES
(1, 'hkj', '5434', 'jhhgj', 7654),
(2, 'dfgdfg', 'dfgdfg', 'dfgdfg', 7654);

-- --------------------------------------------------------

--
-- Table structure for table `profile_pis`
--

CREATE TABLE IF NOT EXISTS `profile_pis` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staffid` int(11) NOT NULL,
  `nic` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_birth` date DEFAULT NULL,
  `nationality` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `full_name` varchar(256) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marital_status` varchar(1) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `staffid` (`staffid`,`nic`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `profile_pis`
--

INSERT INTO `profile_pis` (`id`, `staffid`, `nic`, `date_of_birth`, `nationality`, `full_name`, `marital_status`) VALUES
(4, 111, '871141614V', '2011-02-15', 'Sri Lankan', 'user', 'm'),
(5, 222, '222', '2011-02-24', 'Sri Lankan', 'admin', 's'),
(10, 999, '7655', '2011-01-27', 'Sri Lankan', 'manager', 's'),
(11, 123, '876', '2011-01-11', 'Sri lnkan', 'hr_admin', 's');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE IF NOT EXISTS `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `desc` text COLLATE utf8_unicode_ci NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `progress_cycle` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`id`, `desc`, `start_date`, `end_date`, `progress_cycle`, `title`) VALUES
(4, 'This is a sample project.\r\n\r\nRequirements are as follows:&nbsp;<div><br></div><div>&nbsp;1.sadasdas&nbsp;</div><div>&nbsp;2.dasd asdad</div>', '2011-02-17', '2011-02-28', '1', 'Sample Project'),
(5, 'This is a sample project.\r\n\r\nRequirements are as follows:\r\n\r\n1.sadasdas\r\n2.dasd asdad', '2011-02-17', '2011-02-28', '1', 'Sample Project');

-- --------------------------------------------------------

--
-- Table structure for table `recruitment`
--

CREATE TABLE IF NOT EXISTS `recruitment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staffid` int(11) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `date_of_hire` date NOT NULL,
  `date_of_termination` date DEFAULT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`,`staffid`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `recruitment`
--

INSERT INTO `recruitment` (`id`, `staffid`, `designation_id`, `date_of_hire`, `date_of_termination`, `status`) VALUES
(1, 111, 2, '2011-01-12', NULL, 1),
(2, 222, 1, '2011-01-03', NULL, 1),
(3, 7654, 1, '2011-01-27', NULL, 1),
(4, 999, 1, '2011-01-20', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_type` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `user_type`) VALUES
(111, 'user', 'shaggy', 'user'),
(123, 'hr_admin', 'shaggy', 'hr_admin'),
(222, 'admin', 'shaggy', 'admin'),
(999, 'manager', 'shaggy', 'manager');

-- --------------------------------------------------------

--
-- Table structure for table `user_leave`
--

CREATE TABLE IF NOT EXISTS `user_leave` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staffid` int(11) DEFAULT NULL,
  `manager_staffid` int(11) DEFAULT NULL,
  `type` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `reason` varchar(512) COLLATE utf8_unicode_ci NOT NULL,
  `from` date NOT NULL,
  `to` date NOT NULL,
  `status` bit(1) NOT NULL,
  `request_date` date DEFAULT NULL,
  `approve_date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `user_leave`
--


-- --------------------------------------------------------

--
-- Table structure for table `user_matrix`
--

CREATE TABLE IF NOT EXISTS `user_matrix` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staffid` int(11) NOT NULL,
  `manager_staffid` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Dumping data for table `user_matrix`
--


-- --------------------------------------------------------

--
-- Table structure for table `user_project_assignment`
--

CREATE TABLE IF NOT EXISTS `user_project_assignment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `staffid` int(11) NOT NULL,
  `projectid` int(11) NOT NULL,
  `assign_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=8 ;

--
-- Dumping data for table `user_project_assignment`
--

INSERT INTO `user_project_assignment` (`id`, `staffid`, `projectid`, `assign_date`) VALUES
(5, 111, 3, '2011-02-10'),
(6, 7654, 4, '2011-02-12'),
(7, 111, 4, '2011-02-13');

-- --------------------------------------------------------

--
-- Table structure for table `user_project_status`
--

CREATE TABLE IF NOT EXISTS `user_project_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectid` int(11) NOT NULL,
  `staffid` int(11) NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `comment_date` date NOT NULL,
  `due_date` date NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `user_project_status`
--

INSERT INTO `user_project_status` (`id`, `projectid`, `staffid`, `title`, `comment_date`, `due_date`, `comment`) VALUES
(1, 4, 111, '18/02/2011', '2011-02-13', '2011-02-18', 'Sample<br><br>'),
(2, 4, 111, 'Some ', '2011-02-13', '2011-02-18', 'Status'),
(3, 4, 111, 'asd', '2011-02-13', '2011-02-24', 'asd');

-- --------------------------------------------------------

--
-- Table structure for table `user_training`
--

CREATE TABLE IF NOT EXISTS `user_training` (
  `id` int(11) NOT NULL,
  `due_date` date NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  `staffid` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `approved_date` date NOT NULL,
  `request_date` date NOT NULL,
  `manager_id` int(11) NOT NULL,
  `title` varchar(128) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_training`
--

INSERT INTO `user_training` (`id`, `due_date`, `reason`, `staffid`, `status`, `approved_date`, `request_date`, `manager_id`, `title`) VALUES
(0, '2011-02-01', 'BB', 7654, 0, '0000-00-00', '2011-02-04', 7654, 'Aa'),
(0, '2011-02-16', 'DD', 7654, 0, '0000-00-00', '2011-02-04', 7654, 'CCC');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`id`) REFERENCES `profile_pis` (`staffid`) ON UPDATE CASCADE;

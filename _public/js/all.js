

$(document).ready(function() {

//Text area editor
    $("#input").cleditor({
        controls:     // controls to add to the toolbar
        "bold underline italic | undo redo | " +
            " | cut copy paste pastetext | print source",
    });

//activate the datepicker for the forms
    $(function() {
            $( ".datepicker" ).datepicker({ dateFormat: 'yy-mm-dd' });
        });
    });
